#pragma once

#include <string>
#include <vector>

namespace cliterface {

class Message {
public:
    using args_type = std::vector<std::string>;

private:
    int command_number_;
    std::string command_name_;
    args_type command_args_;
    Message(int number, std::string name, args_type args);

public:
    const static inline char SEPARATOR = '\n';
    const static inline int NO_CMDNUM  = -1;

    [[nodiscard]] int command_number() const {
        return command_number_;
    }
    [[nodiscard]] auto command_name() const {
        return command_name_;
    }
    args_type & command_args() {
        return command_args_;
    }

    const args_type & command_args() const {
        return command_args_;
    }

    std::string unparse() const;

    static Message parse(std::string buf);
};
}  // namespace cliterface
