#pragma once

#include <memory>
#include <string>
#include <list>
#include <helpers/Singleton.hpp>

namespace cliterface {

class Listener {
  struct _Impl;
  std::unique_ptr<_Impl> _pImpl;
  std::string _id;
public:
  const std::string& id();
  Listener(const std::string& host, uint16_t port);
  ~Listener();
  void run_until_terminate();

private:
    void do_accept();
};



}  // namespace cliterface
