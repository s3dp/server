#include "Serializer.hpp"

#include <json/json.h>

namespace cliterface {
std::string Serializer::marshall(HandlerOutput input) {
    Json::Value result;
    result["status_code"] = input.status_code;
    result["message"]     = input.message.value_or("null");
    result["type"]        = input.type;
    auto & num            = input.num;
    if (num.has_value()) {
        result["num"] = num.value();
    } else {
        result["num"] = "null";
    }
    return result.toStyledString();
}

}  // namespace cliterface
