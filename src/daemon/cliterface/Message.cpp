#include <cliterface/Message.hpp>

#include <sstream>

namespace cliterface {
Message::Message(int number, std::string name, Message::args_type args) :
    command_number_{number},
    command_name_{std::move(name)},
    command_args_{std::move(args)} {
}

std::string Message::unparse() const {
    std::stringstream ss;
    if (command_number() >= 0) {
        ss << command_number();
        ss << " ";
    }
    ss << command_name();
    for (const auto & item : command_args()) {
        ss << " ";
        ss << item;
    }
    return ss.str();
}

Message Message::parse(std::string buf) {  // NOLINT(performance-unnecessary-value-param)
    auto impl_cmdnum_only = [](int cmdnum) {
        args_type empty_args;
        return Message{cmdnum, std::string{""}, std::move(empty_args)};
    };
    auto impl_noargs_nocmdnum = [](auto && tokens) {
        std::string cmd = tokens[0];
        args_type empty_args;
        return Message{NO_CMDNUM, std::move(cmd), std::move(empty_args)};
    };
    auto impl_args_or_cmdnum = [](auto && tokens) {
        using namespace std;
        string first = move(tokens[0]);
        tokens.erase(begin(tokens));
        int cmdnum       = NO_CMDNUM;
        bool stoi_failed = false;
        try {
            cmdnum = stoi(first);
        } catch (exception &) {
            stoi_failed = true;
        }
        string command;
        if (stoi_failed) {
            command = move(first);
        } else {
            command = move(tokens[0]);
            tokens.erase(begin(tokens));
        }
        return Message{cmdnum, move(command), forward<decltype(tokens)>(tokens)};
    };
    using namespace std;
    istringstream iss(buf);
    vector<string> tokens;
    string sub;
    while (iss >> sub) {
        tokens.push_back(sub);
    }
    switch (tokens.size()) {
        case 0:
            throw runtime_error("could not parse input as command");
        case 1: {
            try {
                int cmdnum = std::stoi(tokens[0]);
                return impl_cmdnum_only(cmdnum);
            } catch (std::exception &) {
                return impl_noargs_nocmdnum(move(tokens));
            }
        }
        default:
            return impl_args_or_cmdnum(move(tokens));
    }
}
}  // namespace cliterface
