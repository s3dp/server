#pragma once

#include <cliterface/Dispatcher.hpp>

namespace cliterface {

class Serializer {
public:
    std::string marshall(HandlerOutput input);
};
}  // namespace cliterface
