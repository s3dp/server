

#include <cliterface/Dispatcher.hpp>
#include <sstream>
namespace cliterface {




namespace {
const std::string PREDEF_NO_MATCHING_HANDLER = "_service_no_matching_handler";
const std::string PREDEF_CMD_NUMBER_MISMATCH = "_service_cmdnum_mismatch";
}  // namespace


using std::declval;

HandlerOutput Dispatcher::helpCallback ( const HandlerInput & input ) {
    std::stringstream res;
    res << "Available commands:" << std::endl;
    for(auto& handler: _registry->contents()) {
      res << handler.first << std::endl;
    }
    return HandlerOutput{
        .status_code = 0,
        .type        = "cmd-result",
        .message     = res.str(),
        .num         = input.number
    };
};


Dispatcher::_HandlerRegistry::_HandlerRegistry() : _contents{} {
}

void Dispatcher::_HandlerRegistry::registerHandler ( const key_type & key, const value_type & value ) {
    _contents[key] = value;
}



Dispatcher::Dispatcher() :
  helpers::Singleton<Dispatcher>(),
  _registry{std::make_unique<Dispatcher::_HandlerRegistry>(Dispatcher::_HandlerRegistry::initial_instance()) }
{
  auto helpCB = std::bind(&Dispatcher::helpCallback, this, std::placeholders::_1);
  registerHandler("help", helpCB);
}
Dispatcher::return_type Dispatcher::operator() (
    const Message & message,
    HandlerInput && additional ) { // NOLINT(performance-unnecessary-value-param)
    auto query         = message.command_name();
    auto & handler_map = _registry->contents();

    auto iter = handler_map.find ( query );
    if ( iter != handler_map.end() ) {
        auto & handler = iter->second;  // TODO: is this a copy of std::function?
        return handler ( {additional.number, message.unparse(), message.command_args() } );
    }
    return handler_not_found ( message, std::move ( additional ) );
}

Dispatcher::return_type Dispatcher::handler_not_found (
  const Message & message,
  HandlerInput && additional
) {
  std::ignore = message;
  return HandlerOutput{
    .status_code = -1,
    .type        = "cmd-result",
    .message     = "unable to find hanlder for command",
    .num         = additional.number
  };
}

Dispatcher::return_type Dispatcher::cmdnum_mismatch (
  const Message & message,
  HandlerInput && additional
) {
  std::ignore = message;
  return HandlerOutput{
    .status_code = -1,
    .type        = "cmd-result",
    .message     = "unexpected command number value",
    .num         = additional.number
  };
}

void Dispatcher::registerHandler ( const key_type & key, const value_type & value ){
  _registry->registerHandler(key, value);
}


}  // namespace cliterface
