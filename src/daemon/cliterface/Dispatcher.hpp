#pragma once

#include <functional>
#include <memory>
#include <optional>
#include <helpers/Singleton.hpp>
#include <cliterface/Message.hpp>
namespace cliterface {



struct HandlerOutput {
    int status_code = 0;
    std::string type = "";
    std::optional<std::string> message = std::nullopt;
    std::optional<unsigned int> num = std::nullopt;
};

struct HandlerInput {
    std::optional<unsigned int> number = std::nullopt;
    std::string message = "";
    std::vector<std::string> args = {};
};

using handler_type = std::function<HandlerOutput ( const HandlerInput & ) >;

using cli_error_type = HandlerOutput;


class Dispatcher: public helpers::Singleton<Dispatcher> {
private:
    using key_type   = std::string;
    using value_type = handler_type;
    class _HandlerRegistry;
    std::unique_ptr<_HandlerRegistry> _registry;

    class _HandlerRegistry  {
      std::unordered_map<key_type, value_type> _contents;
    public:
      static _HandlerRegistry initial_instance() {
        return _HandlerRegistry{};
      }
      auto & contents() {
        return _contents;
      }
      void registerHandler ( const key_type & key, const value_type & value );
    private:
      _HandlerRegistry();
    };
public:
    Dispatcher();

    using return_type = HandlerOutput;

    void registerHandler ( const key_type & key, const value_type & value );


    return_type operator() ( const Message & message, HandlerInput && additional );

    return_type handler_not_found ( const Message & message, HandlerInput && additional );

    return_type cmdnum_mismatch ( const Message & message, HandlerInput && additional );
protected:
  HandlerOutput helpCallback ( const HandlerInput & input );
};
}  // namespace cliterface
