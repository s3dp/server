

#include <boost/asio.hpp>
#include <cliterface/Listener.hpp>
#include <helpers/Terminator.hpp>
#include <helpers/GlobalIOService.hpp>
#include <iostream>
#include <optional>
#include <thread>

#include <cliterface/Message.hpp>
#include "Serializer.hpp"

namespace cliterface {


namespace {
  namespace errors {
    const cli_error_type parse_failed = {
      .status_code = -1,
      .type        = "cmd-result",
      .message     = "unable to parse incoming message",
      .num         = std::nullopt
    };
  }
}

using boost::asio::ip::tcp;




class CliSession : public std::enable_shared_from_this<CliSession> {
    struct ClientId
    {
        std::optional<tcp::endpoint> endpoint;
    };
    struct ClientSessionData
    {
        int last_command_num;
    };
    tcp::socket socket_;
    boost::asio::streambuf buf_;
    ClientId client_identity_;
    ClientSessionData session_data_;

public:
    explicit CliSession(tcp::socket socket);
    void start() {
        do_read();
    }
    void disconnect() {
        socket_.close();
    }
    ~CliSession();

private:
    static ClientId initial_client_id(const tcp::socket & socket);

    template <typename TReturn>
    void do_write_result(TReturn && result) {
        Serializer se;
        do_write(se.marshall(result));
    }
    template <typename TError>
    void do_write_error(TError && error) {
        Serializer se;
        do_write(se.marshall(error));
    }

    void do_write(const std::string & input);

    void do_read();
};

struct Listener::_Impl
{
  _Impl(boost::asio::io_service& srv, const std::string& host, uint16_t port):
    acceptor(srv, tcp::endpoint(tcp::v4(), port)),
    socket(srv)
  {
    std::ignore = host;
  }
  tcp::acceptor acceptor;
  tcp::socket socket;
};

CliSession::CliSession(tcp::socket socket) :
    socket_{std::move(socket)},
    client_identity_(initial_client_id(socket_)),
    session_data_{.last_command_num = -1} {
}

CliSession::~CliSession() {
    try {
        disconnect();
    } catch (...) {
    }
}

CliSession::ClientId CliSession::initial_client_id(const tcp::socket & socket) {
    boost::system::error_code ec;
    auto ep = socket.remote_endpoint(ec);
    if (ec) {
        return {.endpoint = std::nullopt};
    } else {
        return {.endpoint = ep};
    }
}

void CliSession::do_write(const std::string & input) {
    using boost::system::error_code;
    auto self(shared_from_this());

    boost::asio::async_write(
        socket_, boost::asio::buffer(input.data(), input.length()),
        [self](error_code ec, std::size_t) { std::ignore = ec; });
}

void CliSession::do_read() {
    auto msg_from_buf = [this] {
        using namespace std;
        istream is{&this->buf_};
        string line;
        getline(is, line);
        return Message::parse(line);
    };
    using boost::system::error_code;
    using std::size_t;
    auto self{shared_from_this()};

    boost::asio::async_read_until(
        socket_, buf_, Message::SEPARATOR, [self, msg_from_buf](error_code ec, size_t) {
            if (!ec) {
                std::optional<Message> msg;
                try {
                    msg = msg_from_buf();
                } catch (std::exception &) {
                    msg = std::nullopt;
                }
                std::thread thr([self, msg](){
                if (msg.has_value()) {
                    Message value = std::move(msg.value());

                    int number      = value.command_number();
                    int & last      = self->session_data_.last_command_num;
                    bool is_missing = (number == Message::NO_CMDNUM);
                    bool is_correct = is_missing || (number == (1 + last));
                    if (is_correct && (!is_missing)) {
                        ++last;
                    }
                    auto& dispatcher = Dispatcher::getInstance();
                    HandlerInput additional{
                        .number = ((is_missing || last < 0) ? std::nullopt : std::optional(last))};
                    if (is_correct) {
                      auto result = dispatcher(std::move(value), std::move(additional));
                        self->do_write_result(result);
                    } else {
                        self->do_write_error(Dispatcher::getInstance().cmdnum_mismatch(
                            std::move(value), std::move(additional)));
                    }
                } else {
                    self->do_write_error(errors::parse_failed);
                }

                self->do_read();
              });
              thr.detach();
            }
        });
}

Listener::Listener(const std::string& host, uint16_t port):
  _id{host + ':' + std::to_string(port)}
{
  auto& service = helpers::GlobalIOService::getInstance().service;
  _pImpl = std::make_unique<_Impl>(service, host, port);
  do_accept();
}

void Listener::do_accept() {
    using boost::system::error_code;
    _pImpl->acceptor.async_accept(_pImpl->socket, [this](error_code ec) {
        if (!ec) {
            auto s = std::make_shared<CliSession>(std::move(this->_pImpl->socket));
            s->start();
        }
        this->do_accept();
    });
}

void Listener::run_until_terminate() {
    auto& term = helpers::Terminator::getInstance();
    while (!term.isTerminated()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        // _pImpl->ioService.run_for(std::chrono::milliseconds(100));
    }
}



Listener::~Listener() {
}

}  // namespace cliterface
