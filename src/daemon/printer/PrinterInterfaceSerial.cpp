#include "PrinterInterfaceSerial.hpp"


// #include <boost/asio/io_context.hpp>
// #include <boost/asio/serial_port.hpp>
// #include <boost/asio/buffer.hpp>
// #include <boost/asio/streambuf.hpp>
// #include <boost/asio/read_until.hpp>
// #include <boost/asio/write.hpp>

#include <sstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

// #define __USE_MISC
#include <poll.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/termios.h>
#include <sys/ioctl.h>
#include <linux/serial.h>


namespace printer {
using fd_t = int;
using fd_pair_t = std::pair<fd_t, fd_t>;

void fcntl_addfl(fd_t fd, int flags) {
    fcntl(fd, F_SETFL, fcntl(fd, F_GETFL)|flags);
}

fd_pair_t pipe(){
    fd_t ret[2];
    int res = ::pipe(ret);
    if(res) {
        throw std::runtime_error{"cannot create pipe"};
    }
    fcntl_addfl(ret[0], O_NONBLOCK);
    return {ret[0], ret[1]};
}

struct Pipe{
    fd_pair_t pids;
    Pipe():
        pids(pipe())
    {}

    inline fd_t r_fd() {
        return pids.first;
    }

    inline fd_t w_fd() {
        return pids.second;
    }

    enum class Signal: uint32_t {
        STOP
    };

    Signal read() {
        Signal ret;
        int res = ::read(r_fd(), &ret, sizeof(ret));
        if(res != sizeof(ret)) {
            throw std::runtime_error("pipe read error");
        }
        return ret;
    }

    void write(Signal s) {
        int res = ::write(w_fd(), &s, sizeof(s));
        if(res != sizeof(s)) {
            throw std::runtime_error("pipe write error");
        }
    }

    ~Pipe() {
        ::close(pids.first);
        ::close(pids.second);
    }
};

struct PrinterInterfaceSerial::_Impl{
    std::mutex wMutex;
    std::mutex rMutex;
    fd_t fd;
    Pipe pipe;

    static const size_t iBufSize{4096};
    std::string iBuf;

    _Impl(const std::string& path):
        fd(open(path.c_str(), O_NONBLOCK|O_RDWR|O_NOCTTY))
    {
        if(!isatty(fd)) {
            std::stringstream ss;
            ss<<__PRETTY_FUNCTION__<<": \""<<path<<"\" is not a tty"<<std::flush;
            throw std::runtime_error{ss.str()};
        }
        ioctl(fd, TIOCEXCL, 0);
        tcflush(fd, TCIOFLUSH);
        termios attrs;
        ::tcgetattr(fd, &attrs);
        attrs.c_iflag &= ~(
            IGNBRK    //< ignore break
            | BRKINT  //< convert break to null byte
            | ICRNL   //< no CR to NL translation
            | INLCR   //< no NL to CR translation
            | PARMRK  //< don't mark parity errors or breaks
            | INPCK   //< no input parity check
            | ISTRIP  //< don't strip high bit off
            | IXON    //< no XON/XOFF software flow control
        );

        // Output flags - Turn off output processing
        //
        // no CR to NL translation, no NL to CR-NL translation,
        // no NL to CR translation, no column 0 CR suppression,
        // no Ctrl-D suppression, no fill characters, no case mapping,
        // no local output processing
        //
        // config.c_oflag &= ~(OCRNL | ONLCR | ONLRET |
        //                     ONOCR | ONOEOT| OFILL | OLCUC | OPOST);
        attrs.c_oflag = 0;
        attrs.c_lflag &= ~(
            ECHO      //< echo off
            | ECHONL  //< echo newline off
            | ICANON  //< canonical mode off
            | IEXTEN  //< extended input processing off
            | ISIG    //< signal chars off
        );

        attrs.c_cflag &= ~(CSIZE | PARENB); //< clear current char size mask, no parity checking
        attrs.c_cflag |= CS8; //< force 8 bit input

        attrs.c_cc[VMIN]  = 1; // One input byte is enough to return from read()
        attrs.c_cc[VTIME] = 0; // Inter-character timer off
        ::tcsetattr(fd, TCSANOW, &attrs);

    }
    ~_Impl(){
        close(fd);
    }
};

using IOStatus = PrinterInterfaceSerial::IOStatus;
using BaudRate = PrinterInterfaceSerial::BaudRate;

PrinterInterfaceSerial::PrinterInterfaceSerial(const std::string& path):
    _path(path),
    _pImpl(std::make_unique<PrinterInterfaceSerial::_Impl>(path))
{

}

const std::vector<std::pair<BaudRate, speed_t>> baudmap {
    {BaudRate::BR115200, B115200}
};

std::optional<speed_t> brToTermiosSpeed(BaudRate br) {
    auto res = std::find_if(baudmap.begin(), baudmap.end(), [&](auto& p){
        return p.first == br;
    });
    if(res != baudmap.end()) {
        return (*res).second;
    }
    return std::nullopt;
}

std::optional<BaudRate> termiosSpeedToBr(speed_t br) {
    auto res = std::find_if(baudmap.begin(), baudmap.end(), [&](auto& p){
        return p.second == br;
    });
    if(res != baudmap.end()) {
        return (*res).first;
    }
    return std::nullopt;
}


void PrinterInterfaceSerial::setBaud(BaudRate baud){

    auto spdTermios = brToTermiosSpeed(baud);
    if(spdTermios) {
        termios attrs;

        ::tcgetattr(_pImpl->fd, &attrs);
        ::cfsetspeed(&attrs, *spdTermios);
        ::tcflush(_pImpl->fd, TCIFLUSH);
        ::tcsetattr(_pImpl->fd, TCSANOW, &attrs);
        std::cout<<__PRETTY_FUNCTION__<<": set baudrate using termios"<<std::endl;
    } else {
        int baudNumber = static_cast<int>(baud);
        serial_struct serinfo;
        serinfo.reserved_char[0] = 0;
        if (::ioctl(_pImpl->fd, TIOCGSERIAL, &serinfo)!=0) {
            serinfo.flags &= ~ASYNC_SPD_MASK;
            serinfo.flags |= ASYNC_SPD_CUST;
            serinfo.custom_divisor = (serinfo.baud_base + (baudNumber / 2)) / baudNumber;
            if (serinfo.custom_divisor < 1) {
                serinfo.custom_divisor = 1;
            }
            ::ioctl(_pImpl->fd, TIOCSSERIAL, &serinfo);
            ::ioctl(_pImpl->fd, TIOCGSERIAL, &serinfo);
            std::cout<<__PRETTY_FUNCTION__<<": set baudrate using ioctl"<<std::endl;
        } else {
            std::stringstream ss;
            ss<<__PRETTY_FUNCTION__<<": cannot ioctl TIOCGSERIAL"<<std::flush;
            throw std::runtime_error{ss.str()};
        }
    }

}

BaudRate PrinterInterfaceSerial::getBaud() {
     termios attrs;
    ::tcgetattr(_pImpl->fd, &attrs);
    speed_t spdTermios = ::cfgetospeed(&attrs);
    auto br = termiosSpeedToBr(spdTermios);
    if(!br) {
        throw std::out_of_range{"unknown baudrate"};
    }
    return *br;
}

IOStatus PrinterInterfaceSerial::writeStr(const std::string& what) noexcept {
    std::lock_guard lk{_pImpl->wMutex};
    std::string_view buf{what};
    pollfd fds[2]{
        {
            .fd = _pImpl->fd,
            .events = POLLOUT,
            .revents = 0
        },
        {
            .fd = _pImpl->pipe.r_fd(),
            .events = POLLIN,
            .revents = 0
        }
    };
    while(buf.length()) {
        int nevents = poll(fds, 2, -1);
        if(!nevents) {
            continue;
        }
        if(nevents < 0) {
            std::cout<<__PRETTY_FUNCTION__<<": poll error"<<std::endl;
            return IOStatus::FAIL;
        }
        if(fds[0].revents) {
            if(fds[0].revents&POLLOUT) {
                int nwr = ::write(_pImpl->fd, buf.data(), buf.length());
                if(nwr<0) {
                    std::cout<<__PRETTY_FUNCTION__<<": write error"<<std::endl;
                    return IOStatus::FAIL;
                }
                buf = buf.substr(nwr);
            }
            if(fds[0].revents&(POLLHUP|POLLNVAL|POLLERR)) {
                std::cout<<__PRETTY_FUNCTION__<<": serial error"<<std::endl;
                return IOStatus::FAIL;
            }
        }
        if(fds[1].revents) {
            if(fds[1].revents&POLLIN) {
                try {
                    auto sig = _pImpl->pipe.read();
                    if(sig == Pipe::Signal::STOP) {
                        std::cout<<__PRETTY_FUNCTION__<<": stop"<<std::endl;
                    }
                } catch (...) {
                    std::cout<<__PRETTY_FUNCTION__<<": pipe read error"<<std::endl;
                }
                return IOStatus::FAIL;
            }
            if(fds[1].revents&(POLLHUP|POLLNVAL|POLLERR)) {
                std::cout<<__PRETTY_FUNCTION__<<": pipe error"<<std::endl;
                return IOStatus::FAIL;
            }
        }
    }
    return IOStatus::OK;
}


std::optional<std::string> PrinterInterfaceSerial::readStr() noexcept {
    // std::cout<<__PRETTY_FUNCTION__<<": begin"<<std::endl;
    std::lock_guard lk{_pImpl->rMutex};
    pollfd fds[2]{
        {
            .fd = _pImpl->fd,
            .events = POLLIN,
            .revents = 0
        },
        {
            .fd = _pImpl->pipe.r_fd(),
            .events = POLLIN,
            .revents = 0
        }
    };
    // std::cout<<__PRETTY_FUNCTION__<<": begin loop"<<std::endl;
    while(_pImpl->iBuf.find('\n') == std::string::npos) {
        // std::cout<<__PRETTY_FUNCTION__<<": poll"<<std::endl;
        int nevents = poll(fds, 2, -1);
        if(!nevents) {
            // std::cout<<__PRETTY_FUNCTION__<<": no events"<<std::endl;
            continue;
        }
        if(nevents < 0) {
            std::cout<<__PRETTY_FUNCTION__<<": poll error"<<std::endl;
            return std::nullopt;
        }
        if(fds[0].revents) {
            // std::cout<<__PRETTY_FUNCTION__<<": serial revents"<<std::endl;
            if(fds[0].revents&POLLIN) {
                size_t currLen = _pImpl->iBuf.length();
                if(currLen >= _Impl::iBufSize) {
                    return std::nullopt;
                }
                size_t rLen = _Impl::iBufSize - currLen;
                char tmp[rLen];
                int nr = ::read(_pImpl->fd, tmp, rLen);
                if(nr<0) {
                    std::cout<<__PRETTY_FUNCTION__<<": read error"<<std::endl;
                    return std::nullopt;
                }
                _pImpl->iBuf.append(tmp, nr);

            }
            if(fds[0].revents&(POLLHUP|POLLNVAL|POLLERR)) {
                std::cout<<__PRETTY_FUNCTION__<<": serial error"<<std::endl;
                return std::nullopt;
            }
        }
        if(fds[1].revents) {
            // std::cout<<__PRETTY_FUNCTION__<<": pipe revents"<<std::endl;
            if(fds[1].revents&POLLIN) {
                try {
                    auto sig = _pImpl->pipe.read();
                    if(sig == Pipe::Signal::STOP) {
                        std::cout<<__PRETTY_FUNCTION__<<": stop"<<std::endl;
                    }
                } catch (...) {
                    std::cout<<__PRETTY_FUNCTION__<<": pipe read error"<<std::endl;
                }
                return std::nullopt;
            }
            if(fds[1].revents&(POLLHUP|POLLNVAL|POLLERR)) {
                std::cout<<__PRETTY_FUNCTION__<<": pipe error"<<std::endl;
                return std::nullopt;
            }
        }
    }
    // std::cout<<__PRETTY_FUNCTION__<<": loop finish"<<std::endl;
    auto pos = _pImpl->iBuf.find('\n');
    if(pos == std::string::npos) {
        return std::nullopt;
    }

    std::string ret{_pImpl->iBuf.substr(0, pos)};
    _pImpl->iBuf.erase(0, pos+1);
    return ret;
}

void PrinterInterfaceSerial::stop() noexcept {
    if(_pImpl->rMutex.try_lock()){
        _pImpl->rMutex.unlock();
    } else {
        _pImpl->pipe.write(Pipe::Signal::STOP);
    }
}

bool PrinterInterfaceSerial::good() noexcept {
    // TODO: implement
    return true;
}

PrinterInterfaceSerial::~PrinterInterfaceSerial(){

}

}
