#include <printer/MonitorManager.hpp>
#include <printer/PrinterManager.hpp>
#include <stdexcept>
#include <iostream>


namespace printer {


MonitorManager::Id MonitorManager::addMonitor(const MonitorManager::MonPtr& mon)
{
    Id id{generateId_()};
    MonWrapper wrapped{.id = id, .mon = mon};
    auto listIter = monitors_.emplace(monitors_.end(), std::move(wrapped));
    monitorMapping_[id] = listIter;
    return id;
}

void MonitorManager::delMonitor(const MonitorManager::Id& id)
{
    auto mapIter = monitorMapping_.find(id);
    if(mapIter != monitorMapping_.end()) {
        auto listIter = mapIter->second;
        monitors_.erase(listIter);
        monitorMapping_.erase(mapIter);
    }
}

MonitorManager::Id MonitorManager::generateId_()
{
    if(!monitors_.size()) {
        return 0;
    }
    return monitors_.rbegin()->id + 1;
}


MonitorManager::MonRef MonitorManager::monitor(const MonitorManager::Id& id)
{
    auto iter = monitorMapping_.find(id);
    if(iter == monitorMapping_.end()) {
        throw std::out_of_range{"No monitor with such id"};
    }
    return *iter->second->mon;
}


std::vector<MonitorManager::Id> MonitorManager::monitorIds()
{
    std::vector<Id> ret;
    for(auto& wrapper: monitors_){
        ret.emplace_back(wrapper.id);
    }
    return ret;
}


MonitorManager::MonPtr MonitorManager::monitorPtr(const MonitorManager::Id& id)
{

    auto iter = monitorMapping_.find(id);
    if(iter == monitorMapping_.end()) {
        return nullptr;
    }
    return iter->second->mon;
}




std::vector<MonitorManager::MonPtr> MonitorManager::monitors()
{
    std::vector<MonPtr> ret;
    for(auto& wrapper: monitors_) {
        ret.emplace_back(wrapper.mon);
    }
    return ret;
}



// void MonitorManager::addMonitor(const monitor_type& mon) {
//     _monitors.push_back(mon);
//     mon->on_new_interface().connect(sigc::mem_fun(this, &MonitorManager::addNewInterface));
//     mon->on_remove_interface().connect(sigc::mem_fun(this, &MonitorManager::removeInterface));
//     mon->start();
// }
//
//
// void MonitorManager::addNewInterface(const Monitor::iface_ptr_t& iface) {
//     auto& pm = PrinterManager::getInstance();
//     pm.addPrinter(iface);
//
// }
//
//
// void MonitorManager::removeInterface(const std::string& identifier){
//     auto& pm = PrinterManager::getInstance();
//     try {
//         pm.removePrinter(identifier);
//     } catch (std::exception& e) {
//         // TODO: logging
//         std::cout<<"MonitorManager: cannot remove interface"<<std::endl;
//     }
//
// }

MonitorManager::MonitorManager()
{
}

MonitorManager::~MonitorManager()
{
}


}
