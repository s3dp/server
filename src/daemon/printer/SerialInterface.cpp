#include "SerialInterface.hpp"
#include <chrono>
#include <thread>
#include <iostream>
#include <boost/asio/serial_port.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/write.hpp>
#include <boost/asio/error.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/buffers_iterator.hpp>

#include <sstream>
#include <string>
#include <helpers/Terminator.hpp>
// #include <helpers/GlobalIOService.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/bind/bind.hpp>
#include <boost/asio/strand.hpp>


namespace printer {

struct SerialInterface::_Impl {
    using Serial = boost::asio::serial_port;
    boost::asio::streambuf buffer;
    std::unique_ptr<Serial> serial;
    using IoService = boost::asio::io_context;
    IoService service;
};



SerialInterface::SerialInterface ( const std::string& path ) :
    _path ( path ),
    _identifier ( std::string ( "Serial:" )+path ),
    _pImpl ( std::make_unique<_Impl>() ) {


}


SerialInterface::~SerialInterface() {

}

void SerialInterface::_poll ( size_t ms ) {
    _pImpl->service.run_for(std::chrono::milliseconds(ms));
//     std::this_thread::sleep_for(std::chrono::microseconds(ms));
}
void SerialInterface::_send ( const std::string& cmd ) {
    if ( !_pImpl->serial ) {
        throw std::runtime_error ( "SerialInterface: cannot send (not initialized)" );
    }
    boost::asio::write (
        *_pImpl->serial,
        boost::asio::buffer ( cmd ),
        boost::asio::transfer_all()
    );
    
}

std::string SerialInterface::identifier() const {
    return _identifier;
}


void SerialInterface::_readCallback(const _errCode& err, size_t transferred){
    if(err){
        std::cout<<"SerialInterface: read error ("<<err<<")"<<std::endl;
    }
    if(transferred) {
        std::string s(std::istreambuf_iterator<char>(&this->_pImpl->buffer), std::istreambuf_iterator<char>());
        std::thread thr(
            [s, this]{
                this->_incomingLock.lock();
                std::copy(s.begin(), s.end(), std::back_insert_iterator(this->_incomingBuffer));
                this->_incomingLock.unlock();
                this->_on_incoming();
                
            }
        );
        thr.detach();
        _bindReadCallback();
    }
}

void SerialInterface::_bindReadCallback(){
    _pImpl->buffer.prepare(1024);
    boost::asio::async_read_until(
        *(_pImpl->serial), 
        _pImpl->buffer, 
        '\n',
        boost::bind(
            &SerialInterface::_readCallback,
            this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred
        )
    );
}

void SerialInterface::prepare() {
    using SerialBase = boost::asio::serial_port_base;
    std::cout<<"SerialInterface: prepare (not implemented)"<<std::endl;
    auto& ioService = _pImpl->service;
    _pImpl->serial = std::make_unique<_Impl::Serial> ( ioService, _path );
    // TODO: baudrate autodetect
    _pImpl->serial->set_option ( SerialBase::baud_rate ( 115200 ) );
    this->_bindReadCallback();
    this->flush();
    this->start();
    bool check = false;
//     auto& term = helpers::Terminator::getInstance();
    for (size_t i = 0; ((i<5)&&(!term.isTerminated())) ; ++i) {
        try{
            cmd_blocking("M110 N0", true);
            setCount(0);
            Result res = cmd_blocking("M115");
            std::cout<<res.status<<"|"<<res.response<<std::endl;
            if(!res.status){
                check = true;
                break;
            }
        } catch (const std::exception& e) {
            std::cout<<"PrinterInterface: error ("<<e.what()<<")"<<std::endl;
            check = false;
            break;
        }
        

    }
    cmd_blocking("M155 S1", true);
    this->stop();
    if(!check) {
        throw std::runtime_error("SerialInterface: cannot prepare (cannot reset counter)");
    }
    
    
}


}



