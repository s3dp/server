#pragma once

#include <string>
#include <memory>
#include <printer/PrinterInterface.hpp>
#include <optional>
#include <fstream>


namespace printer {

class Task {


public:
    enum State {
        WAITING, /// Task created but wasn't started
        ACTIVE, /// Task is running right now
        PAUSED, /// Task is paused
        FINISHED, /// Task is finished suckcessfully
        FAILED /// Task failed
    };

    Task(const std::string& path, const std::weak_ptr<PrinterInterface>& printer);

    std::string getPath() const;
    State state() const;

    void pause();

    /**
     * Run task from begin of file
     */
    void run();

    /**
     * Run task from current position
     */
    void proceed();

    /**
     * TODO: add onStateChange signal
     */

private:

    mutable std::weak_ptr<PrinterInterface> _printer;
    const std::string _path;
    std::ifstream _iFile;
    State _state = WAITING;
};

}
