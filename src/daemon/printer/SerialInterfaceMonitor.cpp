#include <printer/SerialInterfaceMonitor.hpp>
#include <thread>
#include <functional>
#include <mutex>
#include <iostream>
#include <sstream>
#include <map>
#include "PrinterInterfaceSerial.hpp"
#include <helpers/TryUniqueLock.hpp>


#include <sys/UDev.hpp>


#include <poll.h>


namespace printer {

using fd_t = int;


using sys::UDev;
using sys::UDevDevice;
using sys::UDevEnumerate;
using sys::UDevMonitor;

struct SerialInterfaceMonitor::_Impl {
    UDev udev;
    std::vector<std::string> devpaths;
    std::mutex mDevpaths;
    std::condition_variable cvConsumer;
    using promise_t = std::promise<SerialInterfaceMonitor::iface_ptr_t>;
//     std::optional<promise_t> prConsumer;

    using SignalGetter = std::function<OnInterfaceSignal&()>;
    SignalGetter signalGetter;

    bool shouldWork{true};
    std::thread thrConsumer{&_Impl::consumer, this};
    std::thread thrEnumerator{&_Impl::enumerator, this};
    std::thread thrWatcher{&_Impl::watcher, this};
    _Impl(const SignalGetter& g): signalGetter(g){}

    ~_Impl(){
        thrConsumer.join();
    }

    void consumer() {
        while(shouldWork){
            std::unique_lock lk{mDevpaths};
            cvConsumer.wait(lk, [this]{
                bool haveData{devpaths.size()>0};
//                 bool haveConsumer{prConsumer};
                return (!shouldWork) || (haveData);
            });
            if(!shouldWork) {
                try {
                    throw std::runtime_error("consumer exiting");
                } catch(...) {
//                     try {
//                         prConsumer->set_exception(std::current_exception());
//                     } catch(...) {} // set_exception() may throw too
                }
                return;
            }
            auto& path = devpaths.front();
            (signalGetter())(std::make_shared<PrinterInterfaceSerial>(path));
//             (*prConsumer).set_value(std::make_shared<PrinterInterfaceSerial>(path));
//             prConsumer = std::nullopt;
            devpaths.erase(devpaths.begin());
        }
    }

    void enumerator() {
        auto enumerate{udev.enumerate()};
        enumerate.addMatchSubsystem("tty");
        enumerate.scanDevices();
        for (auto entry{enumerate.begin()}; entry; entry = entry->next()) {
            UDevDevice dev{udev, entry->getName()};
            if(dev.haveIdPath()){
                std::cout<<__PRETTY_FUNCTION__<<": found device ("<<dev.getNode()<<"|"<<dev.getIdPath()<<")"<<std::endl;
                {
                    std::lock_guard lk{mDevpaths};
                    devpaths.push_back(dev.getNode());
                }
                cvConsumer.notify_one();
            }

        }
    }

    void watcher() {
        auto mon {udev.monitor()};
        mon.addFilterBySubsystem("tty");
        mon.enableReceiving();
        while(shouldWork) {
            auto devOpt = mon.waitAndGet();
            if(!devOpt) {
                continue;
            }
            try {
                auto& dev{*devOpt};
                auto action {dev.getAction()};
                switch (action) {
                    case UDevDevice::Action::ADD:
                    {
                        std::cout<<__PRETTY_FUNCTION__<<": found device ("<<dev.getNode()<<"|"<<dev.getIdPath()<<")"<<std::endl;
                        {
                            std::lock_guard lk{mDevpaths};
                            devpaths.push_back(dev.getNode());
                        }
                        cvConsumer.notify_one();
                        break;
                    }
                    case UDevDevice::Action::REMOVE:
                    case UDevDevice::Action::OFFLINE:
                    {
                        // TODO: what to do?
                        std::cout<<"SerialInterfaceMonitor: remove "<<dev.getNode()<<", what to do here btw?"<<std::endl;
                        break;
                    }
                    default:
                    {
                        std::cout<<__PRETTY_FUNCTION__<<": unknown action "<<dev.getActionStr()<<std::endl;
                        break;
                    }
                }
            } catch (...) {}
        }

    }
};




SerialInterfaceMonitor::SerialInterfaceMonitor() :
    _pImpl ( std::make_unique<_Impl>(std::bind(&InterfaceMonitor::signalOnAdd, this)))
{}


SerialInterfaceMonitor::~SerialInterfaceMonitor() {}



void SerialInterfaceMonitor::stop() {
    std::cout<<"SerialInterfaceMonitor: stop"<<std::endl;
}


}
