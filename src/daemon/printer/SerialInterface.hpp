#pragma once
#include <string>

#include <printer/PrinterInterface.hpp>
#include <boost/system/error_code.hpp>
/**
 * 
 * Интерфейс связи с принтером (конкретизация для последовательного порта)
 * 
 */


namespace printer{
class SerialInterface: public PrinterInterface {
private:
    const std::string _path;
    const std::string _identifier;
    struct _Impl;
    std::unique_ptr<_Impl> _pImpl;
    
    using _errCode = boost::system::error_code;
    void _readCallback(const _errCode& err, size_t transferred);
    void _bindReadCallback();
    
protected:
    virtual void _send(const std::string& cmd) override;
    virtual void _poll(size_t ms) override;
public:
    SerialInterface(const std::string& path);
    ~SerialInterface();
    
    
    
    virtual void prepare() override;
    virtual std::string identifier() const override;
};
}
