#include <printer/TaskManager.hpp>



namespace printer {


void TaskManager::addTask(const std::shared_ptr<Task>& task) {
    TaskWrapper wrp{
        .task = task,
        .id = ++_idCounter
    };
    // TODO: connect onStateChange signal

    _tasks.push_back(wrp);
}

}
