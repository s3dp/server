#pragma once

#include <memory>
#include <list>
#include <unordered_map>
#include <future>
#include <stdexcept>

namespace printer
{

class PrinterInterface;

/**
 * Здесь хранятся и отслеживаются активные интерфейсы
 * Обрабатываются события отключения/подключения
 */
class InterfaceManager final
{
public:
    using Id = uint64_t;
    using Interface = PrinterInterface;
    using InterfacePtr = std::shared_ptr<Interface>;
    using InterfaceRef = Interface&;

    class NewFutureException: public std::runtime_error{};
    class StopException: public std::runtime_error{};

private:
    // Why not to move into pimpl?
    struct Wrapper{
        Id id;
        InterfacePtr interfacePtr;

        InterfaceRef interface() {
            return *interfacePtr;
        }
    };
    using InterfaceList = std::list<Wrapper>;
    using InterfaceListIter = InterfaceList::iterator;
    using InterfaceMap = std::unordered_map<Id, InterfaceListIter>;

    InterfaceList interfaces_;
    InterfaceMap interfaceMap_;

public:
    /**
     * Adds interface into manager
     * @return local id of interface
     * @note Omits future, received by onInterfaceAdd
     */
    Id addInterface(const InterfacePtr& interface);

    /**
     * Just get interface by id
     * @return interface pointer or nullptr if there are no such interface
     */
    InterfacePtr getInterface(const Id& id);

    /**
     * Remove interface from manager.
     * @note Omits future, received by onInterfaceRemove
     */
    void delInterface(const Id& id);

    /**
     * Futures to be omitted on adding/deleting interface
     * @note existing future will be thrown by @ref(NewFutureException) if new one requested before omitting
     * @note in the case when there are no active future the event will be saved internally
     * and omitted later, when future will be requested.
     * @note use @ref(throwEvents) to stop future listeners (on exit, for example)
     * @warning Only one future for each case will be valid at the same time, so you have no option
     * to watch the same event from multiple places. But nothing stops you to handle this
     */
    std::future<Id> onInterfaceAdd();
    std::future<Id> onInterfaceDel();

    /**
     * Throws @ref(StopException) into active futures got by @ref(onInterfaceAdd), @ref(onInterfaceDel)
     */
    void throwEvents();


}; // class InterfaceManager
} // namespace printer
