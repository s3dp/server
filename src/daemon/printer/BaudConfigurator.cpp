
#include "BaudConfigurator.hpp"

#include <list>
#include <string>
#include <thread>
#include <exception>
#include <mutex>

#include <helpers/Executors.hpp>


namespace printer {

using BaudRate = PrinterInterfaceSerial::BaudRate;

namespace {

const std::vector<std::string> responseSuccess{"ok", "echo:ok"};
const std::string cmdKeepalive {"M113\n"};

const std::vector<BaudRate> rates {
    BaudRate::BR115200,
    BaudRate::BR250000
};

bool isSuccess(const std::string& what) {
    for(auto& eval: responseSuccess) {
        if(what.find(eval)!=std::string::npos) {
            return true;
        }
    }
    return false;
}

bool containsSuccess(const std::list<std::string>& where) {
    for(auto& what: where) {
        if(isSuccess(what)) {
            return true;
        }
    }
    return false;
}

} // namespace

void BaudConfigurator::configure() {
    std::list<std::string> responses;
    std::mutex mResponses;

    auto iLambda = [&](){
        bool shouldExit = false;
        while(!shouldExit) {
            std::optional<std::string> res = _pis.readStr();
            if (res) {
                std::lock_guard lk{mResponses};
                responses.push_back(*res);
                if(isSuccess(*res)) {
                    shouldExit = true;
                }
            } else {
                shouldExit = true;
            }
        }
    };

    for (auto br: rates) {
        _pis.setBaud(br);
        std::thread iThr{iLambda};

        bool success{false};
        for(size_t i = 0; (i<5) && (!success); ++i) {
            auto res = _pis.writeStr(cmdKeepalive);
            if(res != PrinterInterface::IOStatus::OK) {
                _pis.stop();
                iThr.join();
                throw std::runtime_error("write error");
            }
            helpers::wait_for(
                std::chrono::seconds(1),
                std::chrono::milliseconds(10),
                [&] {
                    std::lock_guard lk{mResponses};
                    if(containsSuccess(responses)) {
                        success = true;
                        return true;
                    }
                    return false;
                }
            );
            responses.clear();
        }
        if(success) {
            iThr.join();
            return;
        }
        _pis.stop();
        iThr.join();
    }
    throw std::runtime_error("baudrate autodetect error");
}

} // namespace printer
