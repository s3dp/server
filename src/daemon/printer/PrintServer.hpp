#pragma once

#include <memory>

namespace printer {

class TaskManager;
class PrinterManager;
class MonitorManager;
class InterfaceManager;

/**
 * Holds and interconnects all basic modules of printserver.
 *
 * Handles connect/disconnect events, task state change events.
 *
 * Provides initialization (constructs and configures all required monitors, managers, etc)
 *
 */
class PrintServer final {
protected:
    class Impl_;

    std::unique_ptr<InterfaceManager> interfaceMgr_;
    std::unique_ptr<MonitorManager> monitorMgr_;
    std::unique_ptr<PrinterManager> printerMgr_;
    std::unique_ptr<TaskManager> taskMgr_;

    std::unique_ptr<Impl_> pImpl_;


public:

    InterfaceManager& interfaceMgr();
    MonitorManager& monitorMrg();
    PrinterManager& printerMgr();
    TaskManager& taskMgr();

    // TODO: configuration
    PrintServer();
    PrintServer(const PrintServer&) = delete;
    PrintServer(PrintServer&&) = delete;
    PrintServer& operator=(const PrintServer&) = delete;
    PrintServer&& operator=(PrintServer&&) = delete;

    virtual ~PrintServer();

}; // class PrintServer
} // namespace printer
