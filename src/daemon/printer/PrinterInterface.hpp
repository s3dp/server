#pragma once



#include <future>
#include <optional>
#include <string>


namespace printer {
class PrinterInterface {
public:
    enum class IOStatus: uint32_t {
        OK = 0,
        FAIL = 1
    };
    virtual IOStatus writeStr(const std::string& what) noexcept = 0;

    virtual std::optional<std::string> readStr() noexcept = 0;

    virtual void stop() noexcept = 0;
    virtual bool good() noexcept = 0;
    virtual ~PrinterInterface() {}
};
} // namespace printer
