

#include <cliterface/Dispatcher.hpp>


namespace printer::commands {
cliterface::HandlerOutput lsPrintersCallback ( const cliterface::HandlerInput & input );
cliterface::HandlerOutput pCommandCallback ( const cliterface::HandlerInput & input );
cliterface::HandlerOutput pTaskCreateCallback ( const cliterface::HandlerInput & input );
}
