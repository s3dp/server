#pragma once

#include <memory>
#include <list>
#include <unordered_map>
#include <vector>
#include <sigc++/signal.h>


namespace printer {

class InterfaceManager;
class InterfaceMonitor;

class MonitorManager final {
public:
    using MonRef = InterfaceMonitor&;
    using MonPtr = std::shared_ptr<InterfaceMonitor>;
    using Id = size_t;
private:
    struct MonWrapper{
        Id id;
        MonPtr mon;
    };

    using MonList = std::list<MonWrapper>;
    using MonListIter = MonList::iterator;
    using MonMap = std::unordered_map<Id, MonListIter>;
    MonList monitors_;
    MonMap monitorMapping_;

    Id generateId_();

public:
    MonitorManager();
    ~MonitorManager();
    
    Id addMonitor(const MonPtr& mon);
    void delMonitor(const Id& id);
    std::vector<Id> monitorIds();
    std::vector<MonPtr> monitors();
    MonPtr monitorPtr(const Id& id);
    MonRef monitor(const Id& id);


};
}
