#pragma once
#include <memory>
#include <future>
#include <printer/PrinterInterface.hpp>
#include <sigc++/signal.h>



/***
 * Наблюдатель за появлением/исчезновением интерфейсов связи с принтерами
 * 
 */
namespace printer {
class InterfaceMonitor{
public:
    using iface_ptr_t = std::shared_ptr<PrinterInterface>;
    using OnInterfaceSignal = sigc::signal<void(const iface_ptr_t&)>;

protected:
    OnInterfaceSignal onAddSignal_;


public:

    OnInterfaceSignal& signalOnAdd();
    virtual void stop() = 0;
    virtual ~InterfaceMonitor();
    
};
}
