
#include "PrintServer.hpp"

#include "SerialInterfaceMonitor.hpp"
#include "InterfaceManager.hpp"
#include "MonitorManager.hpp"
#include "PrinterManager.hpp"
#include "TaskManager.hpp"



namespace printer {

class PrintServer::Impl_{
protected:
    PrintServer& ps_;
public:
    Impl_(PrintServer& ps): ps_(ps) {
    }


};


void initMonitors(MonitorManager& mon) {
    mon.addMonitor(std::make_shared<SerialInterfaceMonitor>());
}

void connectMonitors(InterfaceManager& im, MonitorManager& mm) {
    // Тут прикрутить обработку событий im и их перенаправление в mm
}

void connectInterfaces(PrinterManager& pm, InterfaceManager& im) {
    // Тут прикрутить обработку подключений/отключений из im и их перенаправление в pm
    // Обработчик на данном этапе должен распознавать и проверять принтер на совместимость,
    // а после добавлять в pm
}

void connectTaskManager(TaskManager& tm, InterfaceManager& im) {
    // При подключении/отключении интерфейсов уведомлять TaskManager
    // Это нужно, чтобы отрабатывать отключения принтеров во время печати.
}

PrintServer::PrintServer():
    interfaceMgr_(std::make_unique<InterfaceManager>()), // Нужно ли разделять?
    monitorMgr_(std::make_unique<MonitorManager>()),
    printerMgr_(std::make_unique<PrinterManager>()),
    taskMgr_(std::make_unique<TaskManager>()),
    pImpl_(std::make_unique<Impl_>(*this)) // А можно как-то завернуть покрасивее? А нужно ли?
{
    initMonitors(*monitorMgr_);
    connectMonitors(*interfaceMgr_, *monitorMgr_);
    connectInterfaces(*printerMgr_, *interfaceMgr_);
    connectTaskManager(*taskMgr_, *interfaceMgr_);

}

PrintServer::~PrintServer() {

}

}
