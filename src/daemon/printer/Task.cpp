#include <printer/Task.hpp>
#include <iostream>

namespace printer{


Task::Task(const std::string& path, const std::weak_ptr<PrinterInterface>& printer):
_printer(printer),
_path(path),
_iFile(path)
{
    if(_iFile.is_open()) {
        std::cout<<"Task::Task cannot open file "<<_path<<std::endl;
        std::cout<<"Changing state to FAILED"<<std::endl;
        _state = FAILED;
    }
}



}
