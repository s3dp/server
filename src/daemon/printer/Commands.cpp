
#include "Commands.hpp"
#include <printer/PrinterManager.hpp>
#include <printer/Task.hpp>
#include <printer/TaskManager.hpp>
#include <sstream>



namespace printer::commands {
  cliterface::HandlerOutput lsPrintersCallback ( const cliterface::HandlerInput & input ) {
    auto& prnMgr = PrinterManager::getInstance();
    auto printers = prnMgr.getPrinters();
    std::stringstream res;
    res << "available printers:" << std::endl;
    for(auto& printer: printers) {
      res << printer << std::endl;
    }
    return cliterface::HandlerOutput {
      .status_code = 0,
      .type        = "cmd-result",
      .message     = res.str(),
      .num         = input.number
    };
  }

  cliterface::HandlerOutput pCommandCallback ( const cliterface::HandlerInput & input ) {
    auto& prnMgr = PrinterManager::getInstance();
    if(input.args.size() < 2) {
      return cliterface::HandlerOutput {
        .status_code = -1,
        .type        = "cmd-result",
        .message     = "printer not specified",
        .num         = input.number
      };
    }
    auto prnName = input.args[0];
    auto prn = prnMgr.findPrinter(prnName);
    if(!prn || !prn->iface) {
      return cliterface::HandlerOutput {
        .status_code = -1,
        .type        = "cmd-result",
        .message     = "printer " + prnName + " not found",
        .num         = input.number
      };
    }
    auto& prnIface = prn->iface;
    std::string prnCmd;
    for (auto arg = input.args.cbegin() + 1; arg != input.args.cend(); ++arg) {
      prnCmd += *arg;
      if(arg +1 != input.args.end()) {
        prnCmd += ' ';
      }
    }
    auto res = prnIface->cmd_blocking(prnCmd);
    return cliterface::HandlerOutput {
      .status_code = res.status,
      .type        = "cmd-result",
      .message     = res.response,
      .num         = input.number
    };
  }

  cliterface::HandlerOutput pTaskCreateCallback ( const cliterface::HandlerInput & input ) {
    auto& args = input.args;
    auto& prnMgr = PrinterManager::getInstance();
    if(args.size() != 3) {
      std::stringstream res;
      res<<"usage:"<<std::endl;
      res<<"\t"<<args[0]<<" <printer> <path>"<<std::endl;
      return cliterface::HandlerOutput {
        .status_code = -1,
        .type        = "cmd-result",
        .message     = res.str(),
        .num         = input.number
      };
    }
    auto printer = prnMgr.findPrinter(args[1]);
    if(!printer){
        return cliterface::HandlerOutput {
          .status_code = -2,
          .type        = "cmd-result",
          .message     = std::string("printer not found"),
          .num         = input.number
        };
    }
    auto task = std::make_shared<Task>(args[2], printer->iface);
    auto& taskMgr = TaskManager::getInstance();
    taskMgr.addTask(task);

    return cliterface::HandlerOutput {
      .status_code = -2,
      .type        = "cmd-result",
      .message     = std::string("ok"),
      .num         = input.number
    };

  }


}
