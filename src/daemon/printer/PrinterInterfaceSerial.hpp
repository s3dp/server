#pragma once

#include <printer/PrinterInterface.hpp>

#include <memory>
#include <string>

namespace printer {
class PrinterInterfaceSerial final: public PrinterInterface {
protected:
    const std::string _path;
    struct _Impl;
    std::unique_ptr<_Impl> _pImpl;

public:
    enum class BaudRate: uint32_t {
        BR115200 = 115200,
        BR250000 = 250000
    };

    const std::string& getPath() const {return _path;}

    void setBaud(BaudRate baud);
    BaudRate getBaud();

    PrinterInterfaceSerial(const std::string& path);
    virtual ~PrinterInterfaceSerial();

    virtual IOStatus writeStr(const std::string& what) noexcept;


    virtual std::optional<std::string> readStr() noexcept;

    virtual void stop() noexcept;
    virtual bool good() noexcept;
};
}
