#pragma once
#include <memory>
#include <list>
#include <unordered_map>
#include <printer/PrinterInterface.hpp>
#include <helpers/Singleton.hpp>

namespace printer{
class PrinterManager final {
public:
    using PrinterWrapper = std::shared_ptr<PrinterInterface>;
    PrinterManager();
private:

    using PrinterList = std::list<PrinterWrapper>;
    using PrinterListIterator = PrinterList::iterator;
    using PrinterMap = std::unordered_map<std::string,PrinterListIterator>;
    PrinterList _prnList;
    PrinterMap _prnMap;


public:

    PrinterWrapper getPrinter(const std::string& id);
    // TODO: make iterator
    std::list<std::string> getPrinterIds();
    void addPrinter(const std::string& id, const PrinterWrapper& iface);
    void removePrinter(const std::string& id);
};
}
