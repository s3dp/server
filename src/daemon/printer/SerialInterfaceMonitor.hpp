#pragma once
#include <printer/InterfaceMonitor.hpp>

/**
 * Наблюдатель за подключением/отключением последовательных портов
 */

namespace printer {
class SerialInterfaceMonitor final : public InterfaceMonitor{
private:
    struct _Impl;
    std::unique_ptr<_Impl> _pImpl;
public:
    SerialInterfaceMonitor();
//     virtual std::future<iface_ptr_t> get();
    virtual void stop();
    virtual ~SerialInterfaceMonitor();
};
}
