#pragma once

#include <printer/PrinterInterfaceSerial.hpp>

namespace printer {

class BaudConfigurator {
protected:
    PrinterInterfaceSerial& _pis;
public:
    BaudConfigurator(PrinterInterfaceSerial& pis);
    virtual ~BaudConfigurator();

    /**
     * Perform baudrate autoconfig
     *
     * @warn Throws std::runtime_error on fail
     */
    void configure();
};

} // namespace printer
