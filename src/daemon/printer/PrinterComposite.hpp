#pragma once

#include <printer/PrinterInterface.hpp>

#include <memory>




namespace printer {
class PrinterComposite final {
protected:
    std::shared_ptr<PrinterInterface> _interface;
    
    std::shared_ptr<PrinterComposite> build(const std::shared_ptr<PrinterInterface>& iface);
}; // class PrinterComposite
}; // namespace printer
