#pragma once

#include <memory>
#include <optional>
#include <list>
#include <set>
#include <map>
#include <helpers/Singleton.hpp>
#include <printer/Task.hpp>

namespace printer{

class TaskManager final {
public:


    std::weak_ptr<Task> getCurrentTaskByPrinter(const std::string& printer);
    std::list<std::weak_ptr<Task>> getTasksByPrinter(const std::string& printer);
    std::list<std::weak_ptr<Task>> getTasksByState(Task::State state);
    /**
     * @return id of task
     */
    uint32_t addTask(const std::string& printer, const std::shared_ptr<Task>& task);
    void removeTask(uint32_t id);


private:
    using TaskWrapper = std::shared_ptr<Task>;
    using TaskList = std::list<TaskWrapper>;
    using TaskListIterator = TaskList::iterator;
    using TaskMap = std::unordered_map<uint32_t,TaskListIterator>;
    using TaskIteratorList = std::list<TaskListIterator>;
    using PrinterTaskMap = std::map<uint32_t, TaskIteratorList>;
    TaskList taskList_;
    TaskMap taskMap_;
    uint32_t idCounter_{0};

};

}
