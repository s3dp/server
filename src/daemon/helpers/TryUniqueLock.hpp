#pragma once
#include <mutex>
#include <functional>



namespace helpers {

template <class M>
class TryUniqueLock {
private:
    std::reference_wrapper<M> _m;
    bool _success;
public:
    TryUniqueLock(M& m):
        _m(m),
        _success(_m.get().try_lock())
    {}
    TryUniqueLock() = delete;
    TryUniqueLock(const TryUniqueLock&) = delete;
    TryUniqueLock(TryUniqueLock&&) = default;
    ~TryUniqueLock() {
        if(_success) {
            _m.get().unlock();
        }
    }

    operator bool() const noexcept {
        return _success;
    }
};
}
