#include <catch2/catch.hpp>
#include <helpers/Terminator.hpp>




TEST_CASE( "Terminator is working", "[helpers]" ) {
    SECTION("Flag working") {
        auto term = helpers::Terminator::create();
        REQUIRE(!term->isTerminated());
        term->terminate();
        REQUIRE(term->isTerminated());
    }

    SECTION("Signal working") {
        auto term = helpers::Terminator::create();
        std::atomic<int> val{0};
        term->on_termination().connect([&]{val = 3;});
        REQUIRE(val == 0);
        term->terminate();
        REQUIRE(val == 3);
    }



}

