
#include <catch2/catch.hpp>
#include <helpers/Singleton.hpp>


class MySingleton: public helpers::Singleton<MySingleton> {
protected:
    int _val{0};
public:
    inline int val(){ return _val; }
    inline void val(int v) { _val = v; }
};

TEST_CASE("Singleton", "[helpers]") {
    SECTION("different instances is the same instance") {
        auto& a = MySingleton::getInstance();
        auto& b = MySingleton::getInstance();
        REQUIRE(a.val()==0);
        REQUIRE(b.val()==0);
        a.val(10);
        REQUIRE(a.val()==10);
        REQUIRE(b.val()==10);
    }
}
