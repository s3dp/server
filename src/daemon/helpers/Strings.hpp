#pragma once

#include <string>

namespace helpers {

std::string toUpper(const std::string& input) {
    std::string res;
    res.reserve(input.size());
    for(auto c: input) {
        res.push_back(std::toupper(c));
    }
    return res;
}

std::string toLower(const std::string& input) {
    std::string res;
    res.reserve(input.size());
    for(auto c: input) {
        res.push_back(std::tolower(c));
    }
    return res;
}

} // namespace helpers
