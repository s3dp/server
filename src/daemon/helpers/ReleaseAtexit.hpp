#pragma once

#include <future>

namespace helpers {

class ReleaseAtexit {
private:
    std::promise<void> _promise;
public:
    ReleaseAtexit() = default;
    ReleaseAtexit(const ReleaseAtexit&) = delete;
    ReleaseAtexit(ReleaseAtexit&&) = default;
    std::shared_future<void> getFuture() {
        return _promise.get_future();
    }
    ~ReleaseAtexit() {
        _promise.set_value();
    }

}; // class ReleaseAtexit

} // namespace helpers
