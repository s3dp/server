#pragma once

namespace helpers {
template <class Child>
class Singleton {
public:
    static Child & getInstance() {
        static Child instance;
        return instance;
    }

    Singleton(const Singleton &) = delete;
    Singleton & operator=(const Singleton) = delete;

protected:
    Singleton() {
    }
    virtual ~Singleton() {
    }
};

}  // namespace helpers
