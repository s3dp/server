#pragma once


#include <chrono>
#include <functional>
#include <thread>

namespace helpers {

template<class Timeout, class Period>
void wait_for(Timeout timeout, Period period, const std::function<bool()>& pred) {
    auto tFinish = std::chrono::high_resolution_clock::now() + timeout;
    while(std::chrono::high_resolution_clock::now() < tFinish) {
        if(pred()) {
            return;
        }
        std::this_thread::sleep_for(period);
    }
}
} // namespace helpers
