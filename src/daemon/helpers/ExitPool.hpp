#pragma once

#include <future>
#include <vector>
#include <mutex>
#include <helpers/Singleton.hpp>

namespace helpers {


class ExitPool: public helpers::Singleton<ExitPool> {
private:
    std::promise<void> _promise;
    std::vector<std::shared_future<void>> _unfinished;
    std::mutex _m;
public:

    void registerTask(const std::shared_future<void>& task) {
        std::unique_lock lk{_m};
        _unfinished.push_back(task);
//         std::cout<<__PRETTY_FUNCTION__<<": register ok"<<std::endl;
    }

    std::shared_future<void> getFuture(){
        return _promise.get_future();
    }

    void exit(){
        _promise.set_value();
//         std::cout<<__PRETTY_FUNCTION__<<": exit"<<std::endl;
        for(auto& task: _unfinished) {
            task.wait();
        }
//         std::cout<<__PRETTY_FUNCTION__<<": exit done"<<std::endl;
    }
}; // class ExitPool


} // namespace helpers

