
#include <catch2/catch.hpp>

#include <sys/Socket.hpp>
#include <thread>


TEST_CASE( "Socket TCP is working", "[socket]" ) {
    using namespace sys;
    SocketListener sl{URI("tcp://0.0.0.0:12345")};


    Socket sock{URI("tcp://127.0.0.1:12345")};
    sock.write("test\n");

}
