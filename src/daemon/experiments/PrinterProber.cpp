#include <iostream>
#include <future>
#include <vector>
#include <mutex>
#include <csignal>
#include <memory>
#include <thread>
#include <filesystem>
#include <optional>
#include <random>
#include <list>
#include <chrono>
#include <regex>
#include <cctype>

#include <helpers/Strings.hpp>

#include <printer/PrinterInterfaceSerial.hpp>
#include <printer/SerialInterfaceMonitor.hpp>


using printer::PrinterInterface;
using printer::PrinterInterfaceSerial;
using printer::SerialInterfaceMonitor;
using BaudRate = PrinterInterfaceSerial::BaudRate;
using helpers::toLower;
using helpers::toUpper;


const std::vector<std::string> responseSuccess{"ok", "echo:ok"};
const std::string cmdKeepalive {"M113\n"};
const std::string cmdFirmwareInfo {"M115\n"};
const std::vector<BaudRate> rates {
    BaudRate::BR115200,
    BaudRate::BR250000
};

bool isSuccess(const std::string& what) {
    for(auto& eval: responseSuccess) {
        if(what.find(eval)!=std::string::npos) {
            return true;
        }
    }
    return false;
}

bool containsSuccess(const std::list<std::string>& where) {
    for(auto& what: where) {
        if(isSuccess(what)) {
            return true;
        }
    }
    return false;
}


template<class Dura, class DuraRecheck>
void wait_or_success(Dura d, DuraRecheck recheck_ms, const std::function<bool()>& pred) {
    auto tFinish = std::chrono::high_resolution_clock::now() + d;
    while(std::chrono::high_resolution_clock::now() < tFinish) {
        if(pred()) {
            return;
        }
        std::this_thread::sleep_for(recheck_ms);
    }
}

void configureBaud(PrinterInterfaceSerial& pis) {
    std::list<std::string> responses;
    std::mutex mResponses;

    auto iLambda = [&](){
        bool shouldExit = false;
        while(!shouldExit) {
            std::optional<std::string> res = pis.readStr();
            if (res) {
                std::lock_guard lk{mResponses};
                responses.push_back(*res);
                if(isSuccess(*res)) {
                    shouldExit = true;
                }
            } else {
                shouldExit = true;
            }
        }
    };

    for (auto br: rates) {
        pis.setBaud(br);
        std::thread iThr{iLambda};

        bool success{false};
        for(size_t i = 0; (i<5) && (!success); ++i) {
            auto res = pis.writeStr(cmdKeepalive);
            if(res != PrinterInterface::IOStatus::OK) {
                pis.stop();
                iThr.join();
                throw std::runtime_error("write error");
            }
            wait_or_success(
                std::chrono::seconds(1),
                std::chrono::milliseconds(10),
                [&] {
                    std::lock_guard lk{mResponses};
                    if(containsSuccess(responses)) {
                        success = true;
                        return true;
                    }
                    return false;
                }
            );
            responses.clear();
        }
        if(success) {
            iThr.join();
            return;
        }
        pis.stop();
        iThr.join();
    }
    throw std::runtime_error("baudrate autodetect error");
}



bool haveMarlin(const std::string& what) {
    auto lWhat{toLower(what)};
    if(lWhat.find("marlin")!=std::string::npos) {
        return true;
    }
    return false;
}

bool haveMarlin(const std::list<std::string>& where) {
    for(auto& what: where) {
        if(haveMarlin(what)){
            return true;
        }
    }
    return false;
}

bool haveUUID(const std::string& what) {
    std::regex exp{"UUID:[a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12}"};
    if(std::regex_search(what, exp)) {
        return true;
    }
    return false;
}

bool haveUUID(const std::list<std::string>& where) {
    for(auto& what: where) {
        if(haveUUID(what)){
            return true;
        }
    }
    return false;
}

std::string extractUUID(const std::string& src) {
    std::regex exp{"UUID:[a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12}", std::regex_constants::icase};
    std::smatch matches;
    if(!std::regex_search(src, matches, exp)) {
        return "";
    }
    if(matches.size()) {
        return matches[0];
    }
    return "";
}



std::string getID(PrinterInterface& pi) {
    std::list<std::string> responses;
    std::mutex mResponses;

    auto iLambda = [&](){
        bool shouldExit = false;
        bool gotUUID{false};
        bool gotConfirm{false};
        while(!shouldExit) {
            std::optional<std::string> res = pi.readStr();
            if (res) {
                std::lock_guard lk{mResponses};
                responses.push_back(*res);
                if(haveUUID(*res)) {
                    gotUUID = true;
                } else if (isSuccess(*res)) {
                    gotConfirm = true;
                }
                if(gotUUID&&gotConfirm) {
                    shouldExit = true;
                }
            } else {
                shouldExit = true;
            }
        }
    };

    std::thread iThr{iLambda};

    auto res = pi.writeStr(cmdFirmwareInfo);
    if(res != PrinterInterface::IOStatus::OK) {
        pi.stop();
        iThr.join();
        throw std::runtime_error("write error");
    }

    bool gotConfirm{false};
    bool gotUUID{false};
    wait_or_success(
        std::chrono::seconds(1),
        std::chrono::milliseconds(10),
        [&] {
            std::lock_guard lk{mResponses};
            if(haveUUID(responses)) {
                gotUUID = true;

            }
            if(containsSuccess(responses)) {
                gotConfirm = true;
            }
            if(gotUUID && gotConfirm) {
                return true;
            }
            return false;
        }
    );
    pi.stop();
    iThr.join();
    for(auto& str: responses) {
        if(haveUUID(str)) {
            return toLower(extractUUID(str));
        }
    }
    return "";
}



bool isMarlin(PrinterInterface& pi) {
    std::list<std::string> responses;
    std::mutex mResponses;

    auto iLambda = [&](){
        bool shouldExit = false;
        bool gotMarlin{false};
        bool gotConfirm{false};
        while(!shouldExit) {
            std::optional<std::string> res = pi.readStr();
            if (res) {
                std::lock_guard lk{mResponses};
                responses.push_back(*res);
                if(haveMarlin(*res)) {
                    gotMarlin = true;
                } else if (isSuccess(*res)) {
                    gotConfirm = true;
                }
                if(gotMarlin&&gotConfirm) {
                    shouldExit = true;
                }
            } else {
                shouldExit = true;
            }
        }
    };

    std::thread iThr{iLambda};

    auto res = pi.writeStr(cmdFirmwareInfo);
    if(res != PrinterInterface::IOStatus::OK) {
        pi.stop();
        iThr.join();
        throw std::runtime_error("write error");
    }

    bool gotConfirm{false};
    bool gotMarlin{false};
    wait_or_success(
        std::chrono::seconds(1),
        std::chrono::milliseconds(10),
        [&] {
            std::lock_guard lk{mResponses};
            if(haveMarlin(responses)) {
                gotMarlin = true;

            }
            if(containsSuccess(responses)) {
                gotConfirm = true;
            }
            if(gotMarlin && gotConfirm) {
                return true;
            }
            return false;
        }
    );
    pi.stop();
    iThr.join();
    for(auto& str: responses) {
        if(haveMarlin(str)) {
            return true;
        }
    }
    return false;
}




void playWith(std::shared_ptr<PrinterInterface> pis) {
    try {
        auto& serial = dynamic_cast<PrinterInterfaceSerial&>(*pis);
        try {
            configureBaud(serial);
        } catch (...) {
            std::cout<<__PRETTY_FUNCTION__<<": unknown baudrate for "<<serial.getPath()<<std::endl;
            return;
        }
        std::cout<<"Successfully configured baudrate for "<<serial.getPath()<<std::endl;
    } catch (...) {
        std::cout<<__PRETTY_FUNCTION__<<"Not a serial"<<std::endl;
    }
    try {
        bool marlin {isMarlin(*pis)};
        std::cout<<__PRETTY_FUNCTION__<<": is Marlin ("<<std::boolalpha<<marlin<<")"<<std::endl;
        std::string uuid{getID(*pis)};
        std::cout<<__PRETTY_FUNCTION__<<": got uuid("<<uuid<<")"<<std::endl;
    } catch (...) {
        std::cout<<__PRETTY_FUNCTION__<<"Failed to get UUID"<<std::endl;
    }


}

int main(int argc, char ** argv) {
    std::ignore = argc;
    std::ignore = argv;

    SerialInterfaceMonitor sim;
    std::promise<printer::InterfaceMonitor::iface_ptr_t> p;
    std::mutex pMutex;
    auto onAdd{[&](const printer::InterfaceMonitor::iface_ptr_t& i){
        std::unique_lock lk{pMutex};
        p.set_value(i);
    }};
    sim.signalOnAdd().connect(onAdd);

    for(;;) {

        auto fut = p.get_future();
        fut.wait();
        std::thread thr{playWith, fut.get()};
        thr.detach();
        std::unique_lock lk{pMutex};
        p = {};
    }

    return 0;
}
