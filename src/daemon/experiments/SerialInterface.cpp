#include <iostream>
#include <future>
#include <vector>
#include <mutex>
#include <csignal>
#include <memory>
#include <thread>
#include <filesystem>
#include <optional>
#include <random>

#include <printer/PrinterInterfaceSerial.hpp>
#include <printer/SerialInterfaceMonitor.hpp>


void reader(std::shared_ptr<printer::PrinterInterface> pis) {
    bool shouldExit = false;
    while(!shouldExit) {
        auto res = pis->readStr();
        if(res) {
            std::cout<<"ok, we got str"<<std::endl;
            std::cout<<"str is \""<<*res<<"\""<<std::endl;
            if(*res == "exit") {
                shouldExit = true;
            }
        } else {
            std::cout<<"input error, stopping reader"<<std::endl;
            shouldExit = true;
        }
    }

}

void writer(std::shared_ptr<printer::PrinterInterface> pis) {
    auto status = pis->writeStr("test\n\ttest\n");
    if(status != printer::PrinterInterface::IOStatus::OK) {
        std::cout<<__PRETTY_FUNCTION__<<": not ok"<<std::endl;
    }
}

void asyncWriter(std::shared_ptr<printer::PrinterInterface> pis) {
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<char> distrib('a', 'z');
    std::stringstream ss;
    const size_t len = 115200/8;
    for(size_t i = 0; i<len; ++i) {
        if(i%1024==0) {
            ss<<'\n';
        }
        ss<<distrib(gen);
    }
    ss<<'\n';
    std::cout<<"writer start"<<std::endl;
    auto fut = std::async ([&](){
        return pis->writeStr(ss.str());
    });
    fut.wait();
    if(fut.get() != printer::PrinterInterface::IOStatus::OK) {
        std::cout<<__PRETTY_FUNCTION__<<": not ok"<<std::endl;
    }
    std::cout<<"writer finish"<<std::endl;
}

void exitWriter(std::shared_ptr<printer::PrinterInterface> pis) {
    auto status = pis->writeStr("exit\n");
    if(status != printer::PrinterInterface::IOStatus::OK) {
        std::cout<<__PRETTY_FUNCTION__<<": not ok"<<std::endl;
    }
}

void setBaud(printer::PrinterInterfaceSerial& pis) {
    pis.setBaud(printer::PrinterInterfaceSerial::BaudRate::BR250000);
}

void playWith(std::shared_ptr<printer::PrinterInterface> pis) {

    try {
        setBaud(dynamic_cast<printer::PrinterInterfaceSerial&>(*pis));
    } catch (...) {}

    std::thread r_thr(reader, pis);
    std::thread w_thr(writer, pis);

    w_thr.join();

    asyncWriter(pis);
    exitWriter(pis);
    r_thr.join();
}



int main(int argc, char ** argv) {
    std::ignore = argc;
    std::ignore = argv;

    printer::SerialInterfaceMonitor sim;
    std::promise<printer::InterfaceMonitor::iface_ptr_t> p;
    std::mutex pMutex;
    auto onAdd{[&](const printer::InterfaceMonitor::iface_ptr_t& i){
        std::unique_lock lk{pMutex};
        p.set_value(i);
    }};
    sim.signalOnAdd().connect(onAdd);

    for(;;) {

        auto fut = p.get_future();
        fut.wait();
        std::thread thr{playWith, fut.get()};
        thr.detach();
        std::unique_lock lk{pMutex};
        p = {};
    }
    
    return 0;
}
