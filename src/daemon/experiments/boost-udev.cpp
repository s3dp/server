#include <stdio.h>
#include <unistd.h>
#include <libudev.h>
#include <mutex>
#include <iostream>

#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/bind.hpp>
#include <boost/asio/placeholders.hpp>

void prnDev(struct udev_device *dev){
    
    if (!dev) {    
        std::cout<<"No device"<<std::endl;
        return;
    }
    std::cout<<"I: ACTION=" << udev_device_get_action(dev) << std::endl;
    std::cout<<"I: DEVNAME=" << udev_device_get_sysname(dev) << std::endl;
    std::cout<<"I: DEVPATH=" << udev_device_get_devpath(dev) << std::endl;
//     std::cout<<"I: MACADDR=" << udev_device_get_sysattr_value(dev, "address") << std::endl;
    std::cout<<"---"<<std::endl;
}


void asyncget(boost::asio::posix::stream_descriptor* sd, struct udev_monitor *mon, const boost::system::error_code& ec){
    if(ec.failed()){
        std::cout << ec.value() << '\n';
        std::cout << ec.category().name() << '\n';
        
    }
    std::cout<<"New dev"<<std::endl;
    struct udev_device *dev = udev_monitor_receive_device(mon);
    if(dev){
        prnDev(dev);
        udev_device_unref(dev);
    }
    sd->async_wait(boost::asio::posix::stream_descriptor::wait_read, boost::bind(asyncget, sd, mon, boost::asio::placeholders::error));
}



int main()
{
	struct udev *udev;
	
   	struct udev_monitor *mon;
	int fd;

	/* create udev object */
	udev = udev_new();
	if (!udev) {
		fprintf(stderr, "Can't create udev\n");
		return 1;
	}
    
	mon = udev_monitor_new_from_netlink(udev, "udev");
	udev_monitor_filter_add_match_subsystem_devtype(mon, "block", NULL);
	udev_monitor_enable_receiving(mon);
	fd = udev_monitor_get_fd(mon);
    boost::asio::io_context io_context;
    boost::asio::posix::stream_descriptor sd(io_context, fd);
    std::cout<<"Init async"<<std::endl;
    
    
    sd.async_wait(boost::asio::posix::stream_descriptor::wait_read, boost::bind(asyncget, &sd, mon, boost::asio::placeholders::error));
    
    while(1){  
        if(io_context.stopped()){
            std::cout<<"restart"<<std::endl;
            io_context.restart();
        }
        io_context.run_one();
    }
    
	/* free udev */
	udev_unref(udev);

	return 0;
}

