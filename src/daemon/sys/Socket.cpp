
#include "Socket.hpp"

#include <stdexcept>
#include <iostream>
#include <regex>
#include <sstream>

namespace sys {

using fd_t = int;

struct Descriptor {
    Descriptor(const Descriptor&) = delete;
    Descriptor(Descriptor&&) = default;
    Descriptor(fd_t v): fd(v) {
        if(fd<=0) {
            throw std::runtime_error{"Non-valid fd"};
        }
    }

    fd_t fd;
};

std::unique_ptr<Descriptor> socketListenerFactory(const URI& uri) {
    std::cout<<uri.scheme()<<std::endl;
    return std::unique_ptr<Descriptor>(
        new Descriptor(10)
    );
}

std::unique_ptr<Descriptor> socketFactory(const URI& uri) {
    std::cout<<uri.scheme()<<std::endl;
    return std::unique_ptr<Descriptor>(
        new Descriptor(10)
    );
}

SocketListener::SocketListener(const URI& uri):
    _uri(uri),
    _descr(socketListenerFactory(uri))
{

}

SocketListener::~SocketListener() {

}


Socket::Socket(std::unique_ptr<Descriptor>&& descr):
    _descr(std::move(descr))
{}

Socket::Socket(const URI& uri):
    _uri(uri),
    _descr(socketFactory(uri))
{}

Socket::~Socket() {


}


Socket::IOStatus Socket::write(const std::string& what) noexcept {
    return IOStatus::FAIL;
}

// Read everything available, wait until something come in.
// Limit max result size if limit is non-zero, otherwise unlimited
std::optional<std::string> Socket::read(size_t limit) noexcept {
    return std::nullopt;
}

std::optional<std::string> Socket::read(ReadCheckPred&) {
    return std::nullopt;
}

void Socket::stop() noexcept {

}
bool Socket::good() noexcept {
    return false;
}

namespace {

const std::string _regexProtocol{"([^\\s\\:]+):.+"};
const std::string _regexAuthority{"([^\\s\\:]+):(\\/\\/[^\\s\\/]+)?.*"};
const std::string _regexAuthPostFilter{"\\/\\/([^\\s]+)"};

std::string getSchemeComponent(const std::string& uri) {
    std::regex r{_regexProtocol};
    auto begin = std::sregex_iterator(uri.begin(), uri.end(), r);
    auto end = std::sregex_iterator();
    if(begin == end) {
        throw std::runtime_error{"String doesn't contains protocol"};
    }
    return std::regex_replace(uri, r, "$1");
}

std::optional<URI::Authority> getAuthorityComponent(const std::string& uri) {
    auto res = std::regex_replace(uri, std::regex(_regexAuthority), "$2");
    res = std::regex_replace(res, std::regex(_regexAuthPostFilter), "$1");
    if(res.empty()) {
        return std::nullopt;
    }
    return res;
}

std::string getPathComponent(const std::string& uri) {
    std::string path{uri};
    return "";
}

// const std::string _regexAUserinfo{"([^\\s\\@]+).*"};
const std::string _regexAuthComponents{
    "([^\\s\\@]+)?\\@?([^\\s\\:]+)\\:?([0-9]+)?"
};

std::optional<std::string> getAuthorityUserinfo(const std::string& a) {
    auto res = std::regex_replace(a, std::regex(_regexAuthComponents), "$1");
    if(res.empty()) {
        return std::nullopt;
    }
    return res;
}

std::string getAuthorityHost(const std::string& a) {
    auto res = std::regex_replace(a, std::regex(_regexAuthComponents), "$2");
    if(res.empty()) {
        throw std::runtime_error{"No host in authority"};
    }
    return res;
}

std::optional<uint16_t> getAuthorityPort(const std::string& a) {
    auto res = std::regex_replace(a, std::regex(_regexAuthComponents), "$2");
    if(res.empty()) {
        return std::nullopt;
    }
    return std::stoul(res);
}

} // namespace


URI::URI(const std::string& v):
    _scheme(getSchemeComponent(v)),
    _authority(getAuthorityComponent(v)),
    _path(getPathComponent(v))
{
    std::cout<<__PRETTY_FUNCTION__<<": "<<str()<<std::endl;


}

std::string URI::str() const {
    std::stringstream ss;
    ss<<_scheme<<':';
    if(_authority) {
        ss<<"//"<<_authority->str();
    }
    ss<<_path;
    return ss.str();
}


std::string URI::Authority::str() const {
    std::stringstream ss;
    if(_userinfo) {
        ss<<(*_userinfo)<<'@';
    }
    ss<<_host;
    if(_port) {
        ss<<':'<<(*_port);
    }
    return ss.str();
}

URI::Authority::Authority(const std::string& str):
    _userinfo(getAuthorityUserinfo(str)),
    _host(getAuthorityHost(str)),
    _port(getAuthorityPort(str))
{
    if(_userinfo) {
        std::cout<<__PRETTY_FUNCTION__<<": userinfo: "<<(*_userinfo)<<std::endl;
    }
    std::cout<<__PRETTY_FUNCTION__<<": host: "<<_host<<std::endl;
    if(_port) {
        std::cout<<__PRETTY_FUNCTION__<<": port: "<<(*_port)<<std::endl;
    }


}

} // namespace sys
