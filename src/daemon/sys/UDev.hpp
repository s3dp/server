#pragma once

#include <libudev.h>
#include <sys/SysTypes.hpp>
#include <string>
#include <string_view>
#include <optional>

namespace sys {

struct UDevMonitor;
struct UDevEnumerate;
struct UDev {
    ::udev * ptr{::udev_new()};
    UDev();
    UDev(const UDev&) = delete;
    UDev(UDev&&) = default;
    ~UDev();

    UDevMonitor monitor();
    UDevEnumerate enumerate();
};





struct UDevDevice {
    ::udev_device * ptr;
    UDevDevice(::udev_device * p);

    UDevDevice(const UDev& udev, const std::string& path);

    ~UDevDevice();

    std::string getNode();

    enum class Action: uint32_t {
        ADD,
        REMOVE,
        OFFLINE
    };
    std::string_view getActionStr();

    Action getAction();

    std::string_view getIdPath();

    bool haveIdPath();
};


struct UDevMonitor {
    ::udev_monitor * ptr;

    UDevMonitor(::udev_monitor * p);
    UDevMonitor(const UDevMonitor&) = delete;
    UDevMonitor(UDevMonitor&&) = default;
    ~UDevMonitor();


    void enableReceiving();

    fd_t fd();

    void addFilterBySubsystem(const std::string& subsys);

    void addFilterBySubsystemAndDevtype(const std::string& subsys, const std::string& devtype);
    std::optional<UDevDevice> waitAndGet();

};


struct UDevListEntry {
    ::udev_list_entry * ptr;
    UDevListEntry(udev_list_entry * e);

    std::optional<UDevListEntry> next();
    std::string getName();
};

struct UDevEnumerate {
    ::udev_enumerate *ptr;
    UDevEnumerate(::udev_enumerate * p);
    ~UDevEnumerate();
    UDevEnumerate(const UDevEnumerate&) = delete;
    UDevEnumerate(UDevEnumerate&&) = default;

    void scanDevices();
    void addMatchSubsystem(const std::string& sys);
    std::optional<UDevListEntry> begin();

};

}
