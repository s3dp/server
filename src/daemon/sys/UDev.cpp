#include "UDev.hpp"

#include <map>
#include <stdexcept>

#include <poll.h>

namespace sys {
UDev::UDev() {
    if(!ptr) {
        throw std::runtime_error{"cannot create udev"};
    }
}

UDev::~UDev() {
    if(ptr) {
        udev_unref(ptr);
        ptr = nullptr;
    }
}

UDevMonitor UDev::monitor() {
    return UDevMonitor{udev_monitor_new_from_netlink(ptr, "udev")};
}

UDevEnumerate UDev::enumerate() {
    return UDevEnumerate{udev_enumerate_new(ptr)};
}



UDevMonitor::UDevMonitor(::udev_monitor * p):
    ptr(p)
{
    if(!ptr) {
        throw std::runtime_error{"cannot create udev monitor"};
    }
}

UDevMonitor::~UDevMonitor() {
    // TODO: what?
    if(ptr) {
        udev_monitor_unref(ptr);
        ptr = nullptr;
    }
}

void UDevMonitor::enableReceiving() {
    // TODO: error handling
    udev_monitor_enable_receiving(ptr);
}

fd_t UDevMonitor::fd() {
    fd_t ret{udev_monitor_get_fd(ptr)};
    if(ret<=0) {
        throw std::runtime_error{"incorrect fd value"};
    }
    return ret;
}

void UDevMonitor::addFilterBySubsystem(const std::string& subsys) {
    // TODO: error handling
    udev_monitor_filter_add_match_subsystem_devtype(ptr, subsys.c_str(), nullptr);
}

void UDevMonitor::addFilterBySubsystemAndDevtype(const std::string& subsys, const std::string& devtype) {
    // TODO: error handling
    udev_monitor_filter_add_match_subsystem_devtype(ptr, subsys.c_str(), devtype.c_str());
}

std::optional<UDevDevice> UDevMonitor::waitAndGet() {
    pollfd pfd {
        .fd = fd(),
        .events = POLLIN,
        .revents = 0
    };
    int nevents = poll(&pfd, 1, -1);
    if (!nevents) {
        return std::nullopt;
    }
    if(nevents<0) {
        throw std::runtime_error{"poll error"};
    }
    if(pfd.revents&POLLIN){
        return udev_monitor_receive_device(ptr);
    }
    if(pfd.revents&(POLLERR|POLLHUP|POLLNVAL)) {
        throw std::runtime_error{"poll bad event received"};
    }
    // TODO: may be other events received. What to do?
    return std::nullopt;
}


UDevDevice::UDevDevice(udev_device* p):
    ptr(p)
{
    if(!ptr) {
        throw std::runtime_error{"cannot receive udev device"};
    }
}

UDevDevice::UDevDevice(const UDev& udev, const std::string& path):
    ptr(udev_device_new_from_syspath(udev.ptr, path.c_str()))
{
    if(!ptr) {
        throw std::runtime_error{"cannot create udev device from syspath"};
    }
}

UDevDevice::~UDevDevice(){
    if(ptr) {
        udev_device_unref(ptr);
        ptr = nullptr;
    }
}

std::string UDevDevice::getNode() {
    return udev_device_get_devnode(ptr);
}

std::string_view UDevDevice::getActionStr() {
    return udev_device_get_action(ptr);
}

namespace {
const std::map<std::string_view,UDevDevice::Action> _actionMap {
    {"add", UDevDevice::Action::ADD},
    {"remove", UDevDevice::Action::REMOVE},
    {"offline", UDevDevice::Action::OFFLINE}
};
}

UDevDevice::Action UDevDevice::getAction() {
    std::string_view action{getActionStr()};
    return _actionMap.at(action);
}

std::string_view UDevDevice::getIdPath() {
    const char * idpath = udev_device_get_property_value(ptr, "ID_PATH");
    if(!idpath) {
        throw std::runtime_error("This device have no id_path");
    }
    return idpath;
}

bool UDevDevice::haveIdPath() {
    return udev_device_get_property_value(ptr, "ID_PATH");
}

UDevEnumerate::UDevEnumerate(::udev_enumerate * p):
    ptr(p)
{
    if(!ptr) {
        throw std::runtime_error("cannot create enumeration");
    }
}
UDevEnumerate::~UDevEnumerate() {
    if(ptr) {
        udev_enumerate_unref(ptr);
        ptr = nullptr;
    }
}

void UDevEnumerate::scanDevices() {
    // TODO: error handling
    udev_enumerate_scan_devices(ptr);
}

void UDevEnumerate::addMatchSubsystem(const std::string& sys) {
    // TODO: error handling
    udev_enumerate_add_match_subsystem(ptr, sys.c_str());
}

std::optional<UDevListEntry> UDevEnumerate::begin() {
    auto res = udev_enumerate_get_list_entry(ptr);
    if(res) {
        return UDevListEntry(res);
    }
    return std::nullopt;
}


UDevListEntry::UDevListEntry(udev_list_entry * e):
    ptr(e)
{
    if(!ptr) {
        throw std::runtime_error("cannot create enumeration");
    }
}

std::optional<UDevListEntry> UDevListEntry::next() {
    auto res = udev_list_entry_get_next(ptr);
    if(res) {
        return UDevListEntry(res);
    }
    return std::nullopt;
}

std::string UDevListEntry::getName() {
    return udev_list_entry_get_name(ptr);
}



}
