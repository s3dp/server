#pragma once

#include <functional>
#include <string>
#include <optional>
#include <memory>


namespace sys {




class URI {
public:
    class Authority{
    private:
        std::optional<std::string> _userinfo;
        std::string _host;
        std::optional<uint16_t> _port;
    public:
        Authority(const std::string& v);
        Authority(const Authority&) = default;
        Authority(Authority&&) = default;

        Authority& operator=(const Authority&) = default;
        Authority& operator=(Authority&&) = default;

        const std::optional<std::string>& userinfo() const {
            return _userinfo;
        }
        void userinfo(const std::string& v) {
            _userinfo = v;
        }

        const std::string& host() const {
            return _host;
        }
        void host(const std::string& v) {
            _host = v;
        }

        std::optional<uint16_t> port() const {
            return _port;
        }
        void port(uint16_t v) {
            _port = v;
        }

        std::string str() const;
    };
protected:
    std::string _scheme;
    std::optional<Authority> _authority;
    std::string _path;

public:
    URI(const std::string&);
    URI(const URI&) = default;
    URI(URI&&) = default;

    const std::string& scheme() const {
        return _scheme;
    }
    void scheme(const std::string& v) {
        _scheme = v;
    }



    const std::optional<Authority>& authority() const {
        return _authority;
    }
    void authority(const Authority& v) {
        _authority = v;
    }

    const std::string& path() const {
        return _path;
    }
    void path(const std::string& v) {
        _path = v;
    }

    std::string str() const;

};

struct Descriptor;


class Socket {
protected:
    std::optional<URI> _uri;
    std::unique_ptr<Descriptor> _descr;

public:
    /**
     * @param params -
     */
    Socket(const URI& uri);
    Socket(std::unique_ptr<Descriptor>&& descr);

    enum class IOStatus: uint32_t {
        OK = 0,
        FAIL,
        STOP
    };

    virtual IOStatus write(const std::string& what) noexcept;

    // Read everything available, wait until something come in.
    // Limit max result size if limit is non-zero, otherwise unlimited
    virtual std::optional<std::string> read(size_t limit = 0) noexcept;

    using ReadCheckPred = std::function<bool(const std::string&)>;
    virtual std::optional<std::string> read(ReadCheckPred&);

    virtual void stop() noexcept;
    virtual bool good() noexcept;
    virtual ~Socket();

};

class SocketListener {
protected:
    std::optional<URI> _uri;
    std::unique_ptr<Descriptor> _descr;
public:
    SocketListener(const URI& uri);

    virtual ~SocketListener();


};


}
