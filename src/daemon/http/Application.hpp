#pragma once

#include <vector>
#include <string>
#include <Poco/Util/ServerApplication.h>


namespace printer {
class PrintServer;
} // namespace printer

namespace http {

class Application: public Poco::Util::ServerApplication {
public:
    Application(const std::shared_ptr<printer::PrintServer>& pise);
protected:
    int main(const std::vector<std::string>& args);


    uint16_t _apiPort{_defaultApiPort};

    static const uint16_t _defaultApiPort;

    std::shared_ptr<printer::PrintServer> printServer_;
}; // class Application

} // namespace http
