#include "Application.hpp"

#include <iostream>

#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/ServerSocket.h>
#include "HandlerFactory.hpp"

#include <helpers/ReleaseAtexit.hpp>
#include <helpers/ExitPool.hpp>

namespace http {

const uint16_t Application::_defaultApiPort{13524};


Application::Application(const std::shared_ptr<printer::PrintServer>& pise):
    printServer_(pise)
{
// TODO: configuration
}

int Application::main(const std::vector<std::string>& args) {

    auto& exitor = helpers::ExitPool::getInstance();
    helpers::ReleaseAtexit onExit;
    exitor.registerTask(onExit.getFuture());
    auto fut = exitor.getFuture();

    Poco::Net::HTTPServer s (
        new HandlerFactory,
        Poco::Net::ServerSocket(_apiPort),
        new Poco::Net::HTTPServerParams
    );

    s.start();
    std::cout << __PRETTY_FUNCTION__ << ": Server started" << std::endl;

    // wait for exitpool
    fut.wait();

    std::cout << __PRETTY_FUNCTION__ << ": Shutting down..." << std::endl;
    s.stop();

    return Application::EXIT_OK;

}

} // namespace http
