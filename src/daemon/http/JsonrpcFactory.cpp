
#include <string>
#include <unordered_map>
#include <functional>
#include <memory>
#include <iostream>

#include "JsonrpcFactory.hpp"
#include "request/JsonRequest.hpp"
#include "request/TestRequest.hpp"


#include <Poco/URI.h>


#include <json/json.h>


namespace http {
namespace {
using HandleCreator = std::function<JsonrpcFactory::Handler*(const Json::Value& params)>;

const std::unordered_map<std::string,HandleCreator> _jsonrpcMethods {
    {"test", [](const Json::Value& params) {return new request::TestRequest{params};}}
};

class BadRequest: public request::JsonRequest {
protected:
    Json::Value _err;
public:

    BadRequest(Json::Value err):
        JsonRequest(Json::Value()),
        _err(err)
    {}
protected:
    virtual void _handle(Request &req, Response &resp) final {
        std::ignore = req;
        resp.setStatus(Poco::Net::HTTPResponse::HTTP_BAD_REQUEST);
        auto& out = resp.send();
        Json::StreamWriterBuilder builder;
        const std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
        writer->write(_err, &out);
        out.flush();
    }
};

} // namespace

JsonrpcFactory::Handler* JsonrpcFactory::handle(const Request& req) {
    const std::string& method{req.getMethod()};

    Json::Value rpcParams;

    if(method == "GET") {
        Poco::URI uri{req.getURI()};
        auto params{uri.getQueryParameters()};
        for(const auto& p: params) {
            rpcParams[p.first] = p.second;
        }

    }
//     else if (method == "POST") {
//         // TODO
//         return nullptr;
//     }
    else {
        // incompatible method
        Json::Value descr;
        Json::Value descrErr;
        descrErr["status"] = -403;
        descrErr["message"] = "Unknown HTTP method";
        descr["jsonrpc"] = "2.0";
        descr["error"] = descrErr;
        return new BadRequest{descr};
    }

    if(rpcParams.isMember("id")) {
        int rpcId{std::stoi(rpcParams["id"].asString())};
        rpcParams["id"] = rpcId;
    }

    if (
        !rpcParams.isMember("jsonrpc")
        || !rpcParams["jsonrpc"].isString()
        || rpcParams["jsonrpc"].asString() != "2.0"
    ) {
        Json::Value descr;
        Json::Value descrErr;
        descrErr["status"] = -405;
        descrErr["message"] = "Unknown or incompatible jsonrpc version";
        descr["jsonrpc"] = "2.0";
        descr["error"] = descrErr;
        if(rpcParams.isMember("id")) {
            descr["id"] = rpcParams["id"].asInt();
        }
        return new BadRequest{descr};
    }

    if (rpcParams.isMember("method") && rpcParams["method"].isString()) {
        std::string func{rpcParams["method"].asString()};
        if(_jsonrpcMethods.find(func) != _jsonrpcMethods.end()) {
            auto creator = _jsonrpcMethods.at(func);
            return creator(rpcParams);
        }
    }

    Json::Value descr;
    Json::Value descrErr;
    descrErr["status"] = -404;
    descrErr["message"] = "Unknown jsonrpc method";
    descr["jsonrpc"] = "2.0";
    descr["error"] = descrErr;
    if(rpcParams.isMember("id")) {
        descr["id"] = rpcParams["id"].asInt();
    }
    return new BadRequest{descr};
}

JsonrpcFactory::~JsonrpcFactory() {

}

} // namespace http
