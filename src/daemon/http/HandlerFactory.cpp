#include "HandlerFactory.hpp"

#include <iostream>

#include "request/TestRequest.hpp"

#include <Poco/URI.h>

namespace http {

HandlerFactory::Handler* HandlerFactory::createRequestHandler(const Request& req) {
//     std::cout << __PRETTY_FUNCTION__ << ": method is " << req.getMethod();
//     const std::string uri{req.getURI()};
    const std::string& method{req.getMethod()};

    Poco::URI uri{req.getURI()};

    const std::string& path{uri.getPath()};

    if(path == "/jsonrpc") {
        return _jsonrpc.handle(req);
    }
    return nullptr;
}

} // namespace http
