#pragma once

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>




namespace http {
class JsonrpcFactory {
public:
    using Handler = Poco::Net::HTTPRequestHandler;
    using Request = Poco::Net::HTTPServerRequest;

    virtual Handler* handle(const Request&);

    virtual ~JsonrpcFactory();

}; // class JsonrpcFactory
} // namespace http
