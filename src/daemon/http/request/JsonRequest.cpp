#include "JsonRequest.hpp"


namespace http::request {



void JsonRequest::handleRequest(Request &req, Response &resp) {
    resp.setContentType("application/json");
    this->_handle(req, resp);
}

} // namespace http::request
