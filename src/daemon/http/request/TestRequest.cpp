#include "TestRequest.hpp"

#include <iostream>
#include <memory>

#include <Poco/Net/HTTPResponse.h>

#include <json/json.h>

namespace http::request {

using Poco::Net::HTTPResponse;

void TestRequest::_handle(Request& req, Response& resp) {
    resp.setStatus(HTTPResponse::HTTP_OK);
    auto& out = resp.send();

    Json::Value result;
    Json::StreamWriterBuilder builder;
    const std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());

    Json::Value resultValue;

    resultValue["method"] = req.getMethod();
    resultValue["uri"] = req.getURI();
    resultValue["jsonrpcRequest"] = _params;

    result["jsonrpc"] = "2.0";
    if(_params.isMember("id")) {
        result["id"] = _params["id"];
    }
    result["result"] = resultValue;
    writer->write(result, &out);

    out.flush();
}

} // namespace http::request
