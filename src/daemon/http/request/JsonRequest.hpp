#pragma once

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

#include <json/json.h>

namespace http::request {

class JsonRequest: public Poco::Net::HTTPRequestHandler {
public:
    using Request = Poco::Net::HTTPServerRequest;
    using Response = Poco::Net::HTTPServerResponse;

    JsonRequest (const Json::Value& params):
        _params(params)
    {}

    virtual void handleRequest(Request &req, Response &resp) final;
protected:
    virtual void _handle(Request &req, Response &resp) = 0;

    Json::Value _params;
};

} // namespace http::request
