#pragma once



#include <http/request/JsonRequest.hpp>

namespace http::request {

class TestRequest: public JsonRequest {
public:
    TestRequest(const Json::Value& params):
        JsonRequest(params)
    {}

protected:
    virtual void _handle(Request &req, Response &resp);
};

} // namespace http::request
