#pragma once

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>

#include <http/JsonrpcFactory.hpp>

namespace http {

class HandlerFactory: public Poco::Net::HTTPRequestHandlerFactory {
protected:
    JsonrpcFactory _jsonrpc;
public:
    using Handler = Poco::Net::HTTPRequestHandler;
    using Request = Poco::Net::HTTPServerRequest;
    virtual Handler* createRequestHandler(const Request&);
};

} // namespace http
