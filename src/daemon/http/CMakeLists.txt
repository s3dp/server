


add_subdirectory(request)

add_library(
3dps-http
STATIC
Application.hpp
Application.cpp
HandlerFactory.hpp
HandlerFactory.cpp
JsonrpcFactory.hpp
JsonrpcFactory.cpp
)

target_link_libraries(3dps-http PRIVATE 3dps-http-request Poco::Util Poco::Net Poco::Foundation)
