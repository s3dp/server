// #include <cliterface/Listener.hpp>
// #include <printer/PrinterManager.hpp>
#include <iostream>
#include <chrono>
#include <thread>
#include <csignal>
#include <future>
#include <functional>
#include <memory>
#include <helpers/Singleton.hpp>
#include <helpers/ExitPool.hpp>
#include <helpers/ReleaseAtexit.hpp>
#include <http/Application.hpp>
#include <printer/PrintServer.hpp>
// #include <printer/MonitorManager.hpp>
// #include <printer/SerialInterfaceMonitor.hpp>
// #include <printer/InterfaceManager.hpp>
// #include <printer/TaskManager.hpp>
// #include <printer/PrinterManager.hpp>


namespace {

void onExit(int signal) {
    auto& exitor = helpers::ExitPool::getInstance();
    exitor.exit();
    std::ignore = signal;
    std::cout<<"EXIT"<<std::endl;
}

}  // namespace


int main(int argc, char * argv[]) {

    using PrintServer = printer::PrintServer;

    std::signal(SIGTERM, onExit);
    std::signal(SIGINT, onExit);
    std::signal(SIGKILL, onExit);
    try {
        std::shared_ptr<PrintServer> pise{std::make_shared<PrintServer>()};




        std::thread thread_http{[&]{
            http::Application app{pise};
            std::cout<<"Starting http api server"<<std::endl;
            app.run(argc, argv);
        }};

        thread_http.join();

    } catch (std::exception & e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }
    return 0;
}
