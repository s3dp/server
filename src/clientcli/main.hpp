#ifndef S3DP_SERVER_MAIN_HPP
#define S3DP_SERVER_MAIN_HPP

#include <boost/asio.hpp>
using boost::asio::ip::tcp;

namespace client::cli::errors {
struct ConnectionAttemptFailed : std::runtime_error {
  ConnectionAttemptFailed() : std::runtime_error{"connection attempt failed"} {}
};
} // namespace client::cli::errors

namespace client::cli {

struct ClientSessionData {
  int last_command_num;
};

class DaemonClient {
  boost::asio::io_service &io_service_;
  tcp::socket daemon_socket_;
  ClientSessionData session_data_;

public:
  DaemonClient(boost::asio::io_service &io_service,
               tcp::resolver::iterator endpoint_iterator)
      : io_service_(io_service),
        daemon_socket_(io_service), session_data_{.last_command_num = -1} {
    connect(std::move(endpoint_iterator));
  }

  auto input(std::string &&line) {
    std::stringstream ss;
    ss << std::to_string(++session_data_.last_command_num);
    ss << " " << std::move(line) << "\n";
    line = ss.str();
    boost::asio::write(daemon_socket_, boost::asio::buffer(line),
                       boost::asio::transfer_all());
    do_debug_read();
  }

  void connect(tcp::resolver::iterator endpoint_iterator) {
    boost::system::error_code ec;
    auto rv =
        boost::asio::connect(daemon_socket_, std::move(endpoint_iterator), ec);
    if (ec || (rv == tcp::resolver::iterator())) {
      throw errors::ConnectionAttemptFailed();
    }
  }
  void disconnect() {
    io_service_.post([this] { daemon_socket_.close(); });
  }

private:
  void do_debug_read() {
    using boost::asio::buffer;
    constexpr size_t debug_length = 4096;
    char reply[debug_length];
    size_t reply_length = daemon_socket_.read_some(buffer(reply, debug_length));
    std::cout.write(reply, reply_length);
    std::cout << "\n";
  }
};
} // namespace client::cli

#endif // S3DP_SERVER_MAIN_HPP
