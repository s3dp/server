

pkg_check_modules(SIGC++ REQUIRED sigc++-3.0)

add_library(sigcppLocal INTERFACE)

target_include_directories(sigcppLocal INTERFACE ${SIGC++_INCLUDE_DIRS})
target_link_libraries(sigcppLocal INTERFACE ${SIGC++_LIBRARIES})

target_link_directories(
    sigcppLocal
    INTERFACE
    ${SIGC++_LIBRARY_DIRS}
)

add_library(sigcpp::sigcpp ALIAS sigcppLocal)
