

find_package(PkgConfig REQUIRED)


find_package(sigc++ REQUIRED)
find_package(jsoncpp REQUIRED)
find_package(Poco REQUIRED COMPONENTS Util Net Foundation)

include(FetchContent)

FetchContent_Declare(
  Catch2
  GIT_REPOSITORY https://github.com/catchorg/Catch2
  GIT_TAG        v2.13.8
)



FetchContent_MakeAvailable(Catch2)
