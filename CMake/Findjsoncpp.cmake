


pkg_check_modules(JSONCPP REQUIRED jsoncpp)


add_library(jsoncppLocal INTERFACE)

target_include_directories(jsoncppLocal INTERFACE ${JSONCPP_INCLUDE_DIRS})
target_link_libraries(jsoncppLocal INTERFACE ${JSONCPP_LIBRARIES})

target_link_directories(
    jsoncppLocal
    INTERFACE
    ${JSONCPP_LIBRARY_DIRS}
)

add_library(jsoncpp::jsoncpp ALIAS jsoncppLocal)

